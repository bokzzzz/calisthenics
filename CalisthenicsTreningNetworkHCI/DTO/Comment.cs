﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalisthenicsTreningNetworkHCI.DTO
{
    class Comment
    {
        private String text;
        private DateTime dateTime;
        private String creator;
        private int id;

        public string Text { get => text; set => text = value; }
        public DateTime DateTime { get => dateTime; set => dateTime = value; }
        public string Creator { get => creator; set => creator = value; }
        public String DateTimeFormat { get => DateTime.ToString("dd.MM.yyyy HH:mm") ; }
        public int Id { get => id; set => id = value; }
    }
}
