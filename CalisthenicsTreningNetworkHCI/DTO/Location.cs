﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalisthenicsTreningNetworkHCI.DTO
{
    public class Location
    {
        private String street;
        private String name;
        private int postCode;

        public string Street { get => street; set => street = value; }
        public string Name { get => name; set => name = value; }
        public int PostCode { get => postCode; set => postCode = value; }
    }
}
