﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HCI_Calisthenics_Trening.DTO
{
    public class User
    {
        private String username;
        private String password;
        private String name;
        private String surname;
        private String theme;
        private String language;
        private String type;

        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string Name { get => name; set => name = value; }
        public string Surname { get => surname; set => surname = value; }
        public string Theme { get => theme; set => theme = value; }
        public string Language { get => language; set => language = value; }
        public string Type { get => type; set => type = value; }
    }
}
