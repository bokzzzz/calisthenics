﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalisthenicsTreningNetworkHCI.DTO
{
    public class Training
    {
        private int spentTime;
        private string creator;
        private int postCode;
        private string street;
        private string locationType;
        private DateTime dateTime;
        private List<ExerciseOnTrening> exercises;
        private int id;
        private String rating;

        public List<ExerciseOnTrening> Exercises { get => exercises; set => exercises = value; }
        public int SpentTime { get => spentTime; set => spentTime = value; }
        public string Creator { get => creator; set => creator = value; }
        public int PostCode { get => postCode; set => postCode = value; }
        public string Street { get => street; set => street = value; }
        public string LocationType { get => locationType; set => locationType = value; }
        public DateTime DateTime { get => dateTime; set => dateTime = value; }
        public int Id { get => id; set => id = value; }
        public String Rating { get => rating; set => rating = value; }
        public String DateTimeFormat { get => DateTime.ToString("dd.MM.yyyy HH:mm"); }
    }
}
