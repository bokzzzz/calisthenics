﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalisthenicsTreningNetworkHCI.DTO
{
    class Statistics
    {
        private int thisMonthTrenings;
        private int previouseMonthTrenings;
        private int thisYearTrenings;
        private int previouseYearTrenings;

        private int thisMonthSpentTime;
        private int previouseMonthSpentTime;
        private int thisYearSpentTime;
        private int previouseYearSpentTime;

        private int thisMonthReps;
        private int thisMonthSets;
        private int thisYearReps;
        private int thisYearSets;
        private int previouseYearReps;
        private int previouseYearSets;
        private int previouseMonthReps;
        private int previouseMonthSets;
        private List<ExerciseOnTrening> exerciseOnTrenings;

        
        public int ThisMonthTrenings { get => thisMonthTrenings; set => thisMonthTrenings = value; }
        public int PreviouseMonthTrenings { get => previouseMonthTrenings; set => previouseMonthTrenings = value; }
        public int ThisYearTrenings { get => thisYearTrenings; set => thisYearTrenings = value; }
        public int PreviouseYearTrenings { get => previouseYearTrenings; set => previouseYearTrenings = value; }
        public int ThisMonthSpentTime { get => thisMonthSpentTime; set => thisMonthSpentTime = value; }
        public int PreviouseMonthSpentTime { get => previouseMonthSpentTime; set => previouseMonthSpentTime = value; }
        public int ThisYearSpentTime { get => thisYearSpentTime; set => thisYearSpentTime = value; }
        public int PreviouseYearSpentTime { get => previouseYearSpentTime; set => previouseYearSpentTime = value; }
        public int ThisMonthReps { get => thisMonthReps; set => thisMonthReps = value; }
        public int ThisMonthSets { get => thisMonthSets; set => thisMonthSets = value; }
        public int ThisYearReps { get => thisYearReps; set => thisYearReps = value; }
        public int ThisYearSets { get => thisYearSets; set => thisYearSets = value; }
        public int PreviouseYearReps { get => previouseYearReps; set => previouseYearReps = value; }
        public int PreviouseYearSets { get => previouseYearSets; set => previouseYearSets = value; }
        public int PreviouseMonthReps { get => previouseMonthReps; set => previouseMonthReps = value; }
        public int PreviouseMonthSets { get => previouseMonthSets; set => previouseMonthSets = value; }
        public List<ExerciseOnTrening> ExerciseOnTrenings { get => exerciseOnTrenings; set => exerciseOnTrenings = value; }
    }
}
