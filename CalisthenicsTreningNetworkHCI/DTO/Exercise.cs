﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalisthenicsTreningNetworkHCI.DTO
{
    public class Exercise
    {
        private int id;
        private string name;
        private int weighted;
        private Int16 level;
        private string type;

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public int Weighted { get => weighted; set => weighted = value; }
        public short Level { get => level; set => level = value; }
        public string Type { get => type; set => type = value; }
    }
}
