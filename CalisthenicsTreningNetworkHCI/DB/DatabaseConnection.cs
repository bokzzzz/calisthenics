﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calisthenics_Trening_Network.DB
{
    class DatabaseConnection
    {

        private static string dbServer;
        private static string databaseName;
        private static string username;
        private static string password;
        private static string port;
        private static string connectionString;
        static DatabaseConnection()
        {
            dbServer = ConfigurationManager.AppSettings["DatebaseServer"];
            databaseName = ConfigurationManager.AppSettings["DatebaseName"];
            username = ConfigurationManager.AppSettings["Username"];
            password = ConfigurationManager.AppSettings["Password"];
            port = ConfigurationManager.AppSettings["Port"];
            connectionString = "Server=" + dbServer + ";Database=" + databaseName + ";Port=" + port + ";UserId=" + username + ";Password=" + password + "; Pooling=True; MinimumPoolSize=10; maximumpoolsize=50;";
        }
        public static MySqlConnection GetConnection() {
            MySqlConnection conn = new MySqlConnection(connectionString);
            conn.Open();
            return conn;
        }
    }
}
