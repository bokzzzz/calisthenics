﻿using Calisthenics_Trening_Network.DAO;
using HCI_Calisthenics_Trening.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Calisthenics_Trening_Network.GUI
{
    /// <summary>
    /// Interaction logic for SignUpWindow.xaml
    /// </summary>
    public partial class SignUpWindow : Window
    {
        public SignUpWindow()
        {
            InitializeComponent();
            themeComboBox.Items.Add("Dark theme");
            themeComboBox.Items.Add("Dark blue theme");
            themeComboBox.Items.Add("Classic theme");
            languageComboBox.Items.Add("English");
            languageComboBox.Items.Add("Srpski");
            languageComboBox.Items.Add("Српски");
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            DialogResult = true;
        }

        private void SignupButton_Click(object sender, RoutedEventArgs e)
        {
            String username = usernameTextBox.Text;
            String name = nameTextBox.Text;
            String surname = surnameTextBox.Text;
            String theme = themeComboBox.SelectedItem != null ? themeComboBox.SelectedItem.ToString() :null;
            String language = languageComboBox.SelectedItem != null ? languageComboBox.SelectedItem.ToString() : null;
            String password = passwordPasswordBox.Password;
            String repeatPassword = repeatPasswordPasswordBox.Password;
            if(String.IsNullOrEmpty(username) || String.IsNullOrEmpty(name) || String.IsNullOrEmpty(surname) || String.IsNullOrEmpty(theme)
                || String.IsNullOrEmpty(language) || String.IsNullOrEmpty(password) || String.IsNullOrEmpty(repeatPassword))
            {
                MessageBox.Show("Please, fill the fields !", "Sign Up Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (password.Equals(repeatPassword))
            {
                UserDAO userDAO = new UserDAO();
                User user = new User { Username = username };
                int result = userDAO.isUsernameExist(user);
                if ( result == 0)
                {
                    user.Name = name;
                    user.Surname = surname;
                    user.Theme = theme;
                    user.Language = language;
                    user.Password = password;
                    if (userDAO.AddUser(user))
                    {
                        MessageBox.Show("Account successfully added", "Sign Up Info", MessageBoxButton.OK, MessageBoxImage.Information);
                        ResetFields();
                        return;
                    }
                    else
                    {
                        MessageBox.Show("unfortunately, the account was not added!", "Sign Up Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }else if( result == 1)
                {
                    MessageBox.Show("Username already exist!", "Sign Up Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                else
                {
                    MessageBox.Show("Please, sign up later!", "Sign Up Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Please, password and repeat password must be the same!", "Sign Up Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }
        private void ResetFields()
        {
            usernameTextBox.Text = "";
            nameTextBox.Text = "";
            surnameTextBox.Text = "";
            passwordPasswordBox.Password = "";
            repeatPasswordPasswordBox.Password = "";
            themeComboBox.SelectedIndex = 0;
            languageComboBox.SelectedIndex = 0;

        }
    }
}
