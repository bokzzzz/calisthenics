﻿using CalisthenicsTreningNetworkHCI.DAO;
using CalisthenicsTreningNetworkHCI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CalisthenicsTreningNetworkHCI.GUI
{
    /// <summary>
    /// Interaction logic for AddChangeLocation.xaml
    /// </summary>
    public partial class AddChangeLocation : Window
    {
        private Location location;
        public AddChangeLocation(Location location, String type)
        {
            this.location = location;
            InitializeComponent();
            if ("Change".Equals(type))
                SetCommponents(location);
        }

        private void SetCommponents(Location location)
        {
            postCodeTextBox.Text = location.PostCode.ToString();
            nameTextBox.Text = location.Name;
            streetTextBox.Text = location.Street;
            addLocationButton.Visibility = Visibility.Collapsed;
            saveLocationButton.Visibility = Visibility.Visible;
        }

        private void postCodeTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void saveLocationButton_Click(object sender, RoutedEventArgs e)
        {
            String name = nameTextBox.Text;
            String postCodeString = postCodeTextBox.Text;
            String street = streetTextBox.Text;
            if (String.IsNullOrEmpty(name) || String.IsNullOrEmpty(postCodeString) || String.IsNullOrEmpty(street))
            {
                new CustomMessageBox(Application.Current.Resources["changeLocationError"].ToString(), Application.Current.Resources["fillFieldsMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
            }
            else
            {
                if (Int32.TryParse(postCodeString, out int postCode))
                {
                    LocationDAO locationDAO = new LocationDAO();
                    Location locationTmp = new Location
                    {
                        Name = nameTextBox.Text,
                        PostCode = postCode,
                        Street = streetTextBox.Text
                    };
                    if (location.Street.Equals(locationTmp.Street) && location.PostCode.Equals(locationTmp.PostCode))
                    {
                        if (locationDAO.ChangeName(locationTmp))
                        {
                            new CustomMessageBox(Application.Current.Resources["changeLocationInfo"].ToString(), Application.Current.Resources["changeLocationInfoMessage"].ToString()).ShowDialog();
                            location.Name = locationTmp.Name;
                            DialogResult = true;
                            Close();
                        }
                        else
                        {
                            DialogResult = false;
                            new CustomMessageBox(Application.Current.Resources["changeLocationError"].ToString(), Application.Current.Resources["notchangeLocationInfoMessage"].ToString()).ShowDialog();
                        }
                    }
                    else
                    {
                        if (locationDAO.IsExist(locationTmp) == 0)
                        {
                            if (locationDAO.ChangeLocation(locationTmp, location))
                            {
                                new CustomMessageBox(Application.Current.Resources["changeLocationInfo"].ToString(), Application.Current.Resources["changeLocationInfoMessage"].ToString()).ShowDialog();
                                location.Street = locationTmp.Street;
                                location.Name = locationTmp.Name;
                                location.PostCode = locationTmp.PostCode;
                                DialogResult = true;
                                Close();
                            }
                            else
                            {
                                DialogResult = false;
                                new CustomMessageBox(Application.Current.Resources["changeLocationError"].ToString(), Application.Current.Resources["notchangeLocationInfoMessage"].ToString()).ShowDialog();
                            }
                        }
                        else
                        {
                            new CustomMessageBox(Application.Current.Resources["changeLocationError"].ToString(), Application.Current.Resources["existLocationInfoMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                        }


                    }
                }
                else
                {
                    new CustomMessageBox(Application.Current.Resources["addLocationError"].ToString(), Application.Current.Resources["validationInputErrorMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                }
            }
        }

        private void addLocationButton_Click(object sender, RoutedEventArgs e)
        {
            String name = nameTextBox.Text;
            String postCodeString = postCodeTextBox.Text;
            String street = streetTextBox.Text;
            if (String.IsNullOrEmpty(name) || String.IsNullOrEmpty(postCodeString) || String.IsNullOrEmpty(street))
            {
                new CustomMessageBox(Application.Current.Resources["addLocationError"].ToString(), Application.Current.Resources["fillFieldsMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
            }
            else
            {
                if (Int32.TryParse(postCodeString, out int postCode))
                {
                    location.Name = name;
                    location.PostCode = postCode;
                    location.Street = street;
                    LocationDAO locationDAO = new LocationDAO();
                    if (locationDAO.IsExist(location) == 0)
                    {
                        if (locationDAO.AddLocation(location))
                        {
                            new CustomMessageBox(Application.Current.Resources["addLocationInfo"].ToString(), Application.Current.Resources["addLocationInfoMessage"].ToString()).ShowDialog();
                            DialogResult = true;
                            Close();
                        }
                        else
                        {
                            DialogResult = false;
                            new CustomMessageBox(Application.Current.Resources["addLocationError"].ToString(), Application.Current.Resources["notAddLocationInfoMessage"].ToString()).ShowDialog();
                        }
                    }
                    else
                    {
                        new CustomMessageBox(Application.Current.Resources["addLocationError"].ToString(), Application.Current.Resources["existLocationInfoMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                    }
                }
                else
                {
                    new CustomMessageBox(Application.Current.Resources["addLocationError"].ToString(), Application.Current.Resources["validationInputErrorMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                }
            }
        }
    }
}
