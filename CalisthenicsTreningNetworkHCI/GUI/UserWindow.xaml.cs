﻿using Calisthenics_Trening_Network.DAO;
using CalisthenicsTreningNetworkHCI;
using CalisthenicsTreningNetworkHCI.DAO;
using CalisthenicsTreningNetworkHCI.DTO;
using CalisthenicsTreningNetworkHCI.GUI;
using HCI_Calisthenics_Trening.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Calisthenics_Trening_Network.GUI
{
    /// <summary>
    /// Interaction logic for UserWindow.xaml
    /// </summary>
    public partial class UserWindow : Window
    {
        private bool isAddTreningClicked = false;
        private bool isManageAccCliked = false;
        private User activeUser;
        private List<ExerciseOnTrening> exerciseOnTrenings = new List<ExerciseOnTrening>();
        private List<Training> myTrainings = new List<Training>();
        private List<Training> globalTrainings = new List<Training>();
        TrainingDAO trainingDAO = new TrainingDAO();
        private Statistics statistics;
        private bool isNotBreakingOperation = true;
        public UserWindow(User user)
        {
            SetThemeAndLanguage(user);
            InitializeComponent();
            addTreningMenuButton_Click(null, null);
            activeUser = user;
            exerviseGrid.ItemsSource = exerciseOnTrenings;
            myTraingsDataGrid.ItemsSource = myTrainings;
            myStaticsButton.Click += myStaticsButton_Click;


        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            DialogResult = true;
        }
        private void SetThemeAndLanguage(User user)
        {
            Console.WriteLine("mjenjam temu");
            Application.Current.Resources.MergedDictionaries.Clear();
            SetTheme(user);
            SetLanguage(user);
        }
        private void SetTheme(User user)
        {
            if ("Dark theme".Equals(user.Theme))
            {
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("/Dict/Dark.xaml", UriKind.Relative) });
            }
            else if ("Classic theme".Equals(user.Theme))
            {

                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("/Dict/Classic.xaml", UriKind.Relative) });
            }
            else
            {
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("/Dict/Blue.xaml", UriKind.Relative) });
            }
        }
        private void SetLanguage(User user)
        {
            if ("English".Equals(user.Language))
            {
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("/Dict/Eng.xaml", UriKind.Relative) });
            }
            else if ("Српски".Equals(user.Language))
            {
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("/Dict/SrbCir.xaml", UriKind.Relative) });
            }
            else
            {
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("/Dict/Srb.xaml", UriKind.Relative) });
            }
        }
        private void ResetTheme()
        {
            Application.Current.Resources.MergedDictionaries.Clear();
            Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("/Dict/Classic.xaml", UriKind.Relative) });
        }

        private void addTreningMenuButton_Click(object sender, RoutedEventArgs e)
        {
            isNotBreakingOperation = false;
            addTreningGrid.Visibility = Visibility.Visible;
            createTreningLabel.Visibility = Visibility.Visible;
            myTrainingsGrid.Visibility = Visibility.Collapsed;
            globalTrainingsGrid.Visibility = Visibility.Collapsed;
            searchGlobalGrid.Visibility = Visibility.Collapsed;
            manageAccountGrid.Visibility = Visibility.Collapsed;
            statsGrid.Visibility = Visibility.Collapsed;
            loadingGrid.Visibility = Visibility.Collapsed;
            statsGrid.Visibility = Visibility.Collapsed;
            
            if (!isAddTreningClicked)
            {
                foreach (Location location in new LocationDAO().GetLocations())
                    locationDataGrid.Items.Add(location);
            }
        }

        private void myTreningsButton_Click(object sender, RoutedEventArgs e)
        {
            isNotBreakingOperation = false;
            addTreningGrid.Visibility = Visibility.Collapsed;
            createTreningLabel.Visibility = Visibility.Collapsed;
            globalTrainingsGrid.Visibility = Visibility.Collapsed;
            myTrainingsGrid.Visibility = Visibility.Visible;
            searchGlobalGrid.Visibility = Visibility.Collapsed;
            manageAccountGrid.Visibility = Visibility.Collapsed;
            statsGrid.Visibility = Visibility.Collapsed;
            loadingGrid.Visibility = Visibility.Collapsed;
            statsGrid.Visibility = Visibility.Collapsed;
            
            offsetForMyTrainingDataGrid = 0;
            myTrainings.Clear();
            trainingDAO.GetTrainings(activeUser, offsetForMyTrainingDataGrid).ForEach(p => myTrainings.Add(p));
            myTraingsDataGrid.Items.Refresh();

        }

        private void globalTreningsButton_Click(object sender, RoutedEventArgs e)
        {
            isNotBreakingOperation = false;
            addTreningGrid.Visibility = Visibility.Collapsed;
            createTreningLabel.Visibility = Visibility.Collapsed;
            myTrainingsGrid.Visibility = Visibility.Collapsed;
            globalTrainingsGrid.Visibility = Visibility.Visible;
            searchGlobalGrid.Visibility = Visibility.Visible;
            manageAccountGrid.Visibility = Visibility.Collapsed;
            statsGrid.Visibility = Visibility.Collapsed;
            loadingGrid.Visibility = Visibility.Collapsed;
            statsGrid.Visibility = Visibility.Collapsed;
            globalTrainings.Clear();
            offsetForGlobalTrainingDataGrid = 0;
            trainingDAO.GetTrainingsSeacrchByCreators("", activeUser.Username, offsetForGlobalTrainingDataGrid).ForEach(p => globalTraingsDataGrid.Items.Add(p));
        }

        private void myStaticsButton_Click(object sender, RoutedEventArgs e)
        { 
            addTreningGrid.Visibility = Visibility.Collapsed;
            createTreningLabel.Visibility = Visibility.Collapsed;
            myTrainingsGrid.Visibility = Visibility.Collapsed;
            globalTrainingsGrid.Visibility = Visibility.Collapsed;
            searchGlobalGrid.Visibility = Visibility.Collapsed;
            manageAccountGrid.Visibility = Visibility.Collapsed;
            loadingGrid.Visibility = Visibility.Visible;
            statsGrid.Visibility = Visibility.Collapsed;
            bestRecordsDataGrid.Items.Clear();
            isNotBreakingOperation = true;
            Task.Run(() =>{
                CalculateStatistics();
                if (isNotBreakingOperation) SetStatisticsLabel();
            });
        }

        private void SetStatisticsLabel()
        {
            myStaticsButton.Dispatcher.Invoke(
            DispatcherPriority.Background,

          new Action(() =>
          {
              thisMonthReps.Content = statistics.ThisMonthReps;
              thisMonthSets.Content = statistics.ThisMonthSets;
              thisMonthSpentTime.Content = GetParseTime(statistics.ThisMonthSpentTime);
              thisMonthTrenings.Content = statistics.ThisMonthTrenings;
              thisYearReps.Content = statistics.ThisYearReps;
              thisYearSets.Content = statistics.ThisYearSets;
              thisYearSpentTime.Content = GetParseTime(statistics.ThisYearSpentTime);
              thisYearTrenings.Content = statistics.ThisYearTrenings;
              //previouse
              previouseMonthReps.Content = statistics.PreviouseMonthReps;
              previouseMonthSets.Content = statistics.PreviouseMonthSets;
              previouseMonthSpentTime.Content = GetParseTime(statistics.PreviouseMonthSpentTime);
              previouseMonthTrenings.Content = statistics.PreviouseMonthTrenings;
              previouseYearReps.Content = statistics.PreviouseYearReps;
              previouseYearSets.Content = statistics.PreviouseYearSets;
              previouseYearSpentTime.Content = GetParseTime(statistics.PreviouseYearSpentTime);
              previouseYearTrenings.Content = statistics.PreviouseYearTrenings;
              //best
              statistics.ExerciseOnTrenings.ForEach(p=> bestRecordsDataGrid.Items.Add(p));
              loadingGrid.Visibility = Visibility.Collapsed;
              statsGrid.Visibility = Visibility.Visible;
          }));
        }

        private void manageAccountButton_Click(object sender, RoutedEventArgs e)
        {
            isNotBreakingOperation = false;
            addTreningGrid.Visibility = Visibility.Collapsed;
            createTreningLabel.Visibility = Visibility.Collapsed;
            myTrainingsGrid.Visibility = Visibility.Collapsed;
            globalTrainingsGrid.Visibility = Visibility.Collapsed;
            searchGlobalGrid.Visibility = Visibility.Collapsed;
            statsGrid.Visibility = Visibility.Collapsed;
            loadingGrid.Visibility = Visibility.Collapsed;
            statsGrid.Visibility = Visibility.Collapsed;
            manageAccountGrid.Visibility = Visibility.Visible;
            if (!isManageAccCliked)
            {
                SetFieldForManageAccount();
                isManageAccCliked = true;
            }
        }
        private void SetFieldForManageAccount()
        {
            usernameTextBox.Text = activeUser.Username;
            usernameTextBox.IsReadOnly = true;
            nameTextBox.Text = activeUser.Name;
            surnameTextBox.Text = activeUser.Surname;
            if (themeComboBox.Items.Count < 1)
            {
                themeComboBox.Items.Add("Dark theme");
                themeComboBox.Items.Add("Dark blue theme");
                themeComboBox.Items.Add("Classic theme");
            }
            if (languageComboBox.Items.Count < 1)
            {
                languageComboBox.Items.Add("English");
                languageComboBox.Items.Add("Srpski");
                languageComboBox.Items.Add("Српски");
            }
            themeComboBox.SelectedItem = activeUser.Theme;
            languageComboBox.SelectedItem = activeUser.Language;
        }
        private void addExerciseButton_Click(object sender, RoutedEventArgs e)
        {
            ExerciseOnTrening exerciseOnTrening = new ExerciseOnTrening();
            Window window = new AddExerciseWindow(exerciseOnTrening, "Add");
            bool isSuccess = window.ShowDialog().Value;
            if (isSuccess)
            {
                bool isExist = exerciseOnTrenings.Exists(x => x.Id == exerciseOnTrening.Id && x.Reps == exerciseOnTrening.Reps);
                if (isExist)
                {
                    ExerciseOnTrening findExercise = exerciseOnTrenings.Find(x => x.Id == exerciseOnTrening.Id && x.Reps == exerciseOnTrening.Reps);
                    findExercise.Sets += exerciseOnTrening.Sets;
                    findExercise.Rest += exerciseOnTrening.Rest;
                    exerviseGrid.Items.Refresh();
                }
                else
                {
                    exerciseOnTrenings.Add(exerciseOnTrening);
                    exerviseGrid.Items.Refresh();
                }
            }
        }

        private void removeExerciseButton_Click(object sender, RoutedEventArgs e)
        {
            if (exerviseGrid.SelectedIndex < 0)
            {
                new CustomMessageBox(Application.Current.Resources["RemoveExerciseTitleErrorMessage"].ToString(), Application.Current.Resources["chooseExerciseErrorMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
            }
            else
            {
                exerciseOnTrenings.Remove(exerviseGrid.SelectedItem as ExerciseOnTrening);
                exerviseGrid.Items.Refresh();
            }
        }

        private void ChangeExerciseButton_Click(object sender, RoutedEventArgs e)
        {
            if (exerviseGrid.SelectedIndex < 0)
            {
                new CustomMessageBox(Application.Current.Resources["ChangeExerciseTitleErrorMessage"].ToString(), Application.Current.Resources["chooseExerciseErrorMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
            }
            else
            {
                ExerciseOnTrening changeExercise = exerviseGrid.SelectedItem as ExerciseOnTrening;
                exerciseOnTrenings.Remove(changeExercise);
                Window window = new AddExerciseWindow(changeExercise, "Change");
                bool isSuccess = window.ShowDialog().Value;
                if (isSuccess)
                {
                    
                    bool isExist = exerciseOnTrenings.Exists(x => x.Id == changeExercise.Id && x.Reps == changeExercise.Reps);
                    if (isExist)
                    {
                        ExerciseOnTrening findExercise = exerciseOnTrenings.Find(x => x.Id == changeExercise.Id && x.Reps == changeExercise.Reps);
                        findExercise.Sets += changeExercise.Sets;
                        findExercise.Rest += changeExercise.Rest;
                        exerviseGrid.Items.Refresh();
                    }
                    else
                    {
                        exerciseOnTrenings.Add(changeExercise);
                        exerviseGrid.Items.Refresh();
                    }
                }
            }
        }

        private void addTreningButton_Click(object sender, RoutedEventArgs e)
        {
            String locationType = locationTypeTextBox.Text;
            String spentTimeString = spentTimeTextBox.Text;
            String dateTimeString = dateTimePicker.Text;
            if (String.IsNullOrEmpty(locationType) || String.IsNullOrEmpty(spentTimeString) || String.IsNullOrEmpty(dateTimeString))
            {
                new CustomMessageBox(Application.Current.Resources["addTrainingErrorMessage"].ToString(), Application.Current.Resources["fillFieldsMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }
            if (exerciseOnTrenings.Count < 1)
            {
                new CustomMessageBox(Application.Current.Resources["addTrainingErrorMessage"].ToString(),Application.Current.Resources["addExerciseErrorMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }
            if (locationDataGrid.SelectedIndex < 0)
            {
                new CustomMessageBox(Application.Current.Resources["addTrainingErrorMessage"].ToString(),Application.Current.Resources["addLocationErrorMessage"].ToString() , CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }
            DateTime dateTime;
            int spentTime;
            if (DateTime.TryParse(dateTimeString, out dateTime) && Int32.TryParse(spentTimeString, out spentTime))
            {
                Location location = locationDataGrid.SelectedItem as Location;
                Training trening = new Training
                {
                    DateTime = dateTime,
                    Exercises = exerciseOnTrenings,
                    LocationType = locationType,
                    Street = location.Street,
                    PostCode = location.PostCode,
                    SpentTime = spentTime,
                    Creator = activeUser.Username
                };
                if (trainingDAO.AddTrening(trening))
                {
                    new CustomMessageBox( Application.Current.Resources["AddTrainingInfoMessage"].ToString(),Application.Current.Resources["succesfullyAddedTrainingMessage"].ToString(), CustomMessageBoxIcon.Information).ShowDialog();
                    ClearFields();
                    return;
                }
                else
                {
                    new CustomMessageBox(Application.Current.Resources["addTrainingErrorMessage"].ToString(),Application.Current.Resources["notAddedTrainingErrorMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                }
            }
            else
            {
                new CustomMessageBox(Application.Current.Resources["addTrainingErrorMessage"].ToString(), Application.Current.Resources["validationInputErrorMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }
        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        private void ClearFields()
        {
            exerciseOnTrenings.Clear();
            exerviseGrid.Items.Refresh();
            locationTypeTextBox.Text = "";
            spentTimeTextBox.Text = "";
        }
        int offsetForMyTrainingDataGrid = 0;
        private void myTraingsDataGrid_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            ScrollViewer scrollViewer = e.OriginalSource as ScrollViewer;
            if (e.VerticalChange > 0)
            {
                if (scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                {
                    offsetForMyTrainingDataGrid++;
                    List<Training> trainings = trainingDAO.GetTrainings(activeUser, offsetForMyTrainingDataGrid * 30);

                    if (trainings.Count == 0)
                    {
                        offsetForMyTrainingDataGrid--;
                        return;
                    }
                    trainings.ForEach(p => myTrainings.Add(p));

                    myTraingsDataGrid.Items.Refresh();
                    myTraingsDataGrid.ScrollIntoView(trainings[0]);

                }


            }
        }

        private void detailsMyTreningsButton_Click(object sender, RoutedEventArgs e)
        {
            if (myTraingsDataGrid.SelectedIndex >= 0)
            {
                Window window = new DetailsMyTraining(myTraingsDataGrid.SelectedItem as Training, activeUser, "my");
                window.ShowDialog();
            }
            else
            {
                new CustomMessageBox(Application.Current.Resources["detailsTraningMessage"].ToString(), Application.Current.Resources["selectTrainingErrorMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
            }
        }

        private void deleteMyTreningsButton_Click(object sender, RoutedEventArgs e)
        {
            if (myTraingsDataGrid.SelectedIndex >= 0)
            {
                if (trainingDAO.removeTrening(myTraingsDataGrid.SelectedItem as Training))
                {
                    myTrainings.Remove(myTraingsDataGrid.SelectedItem as Training);
                    myTraingsDataGrid.Items.Refresh();
                    new CustomMessageBox(Application.Current.Resources["deleteTraningInfoMessage"].ToString(),Application.Current.Resources["deleteSuccessfullyTraningMessage"].ToString(), CustomMessageBoxIcon.Information).ShowDialog();
                }
                else
                {
                    new CustomMessageBox(Application.Current.Resources["deleteTraningInfoMessage"].ToString(), Application.Current.Resources["deleteNotSuccessfullyTraningMessage"].ToString(), CustomMessageBoxIcon.Information).ShowDialog();
                }
            }
            else
            {
                new CustomMessageBox(Application.Current.Resources["deleteTraningMessage"].ToString(), Application.Current.Resources["selectTrainingErrorMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
            }
        }

        private void changeMyTreningsButton_Click(object sender, RoutedEventArgs e)
        {
            if (myTraingsDataGrid.SelectedIndex >= 0)
            {
               bool? isChange= new ChangeTrainingWindow(myTraingsDataGrid.SelectedItem as Training, activeUser).ShowDialog();
                if(isChange.HasValue && isChange.Value)
                {
                    myTraingsDataGrid.Items.Refresh();
                }
            }
            else
            {
                new CustomMessageBox(Application.Current.Resources["changeTraningMessage"].ToString(), Application.Current.Resources["selectTrainingErrorMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
            }
        }
        int offsetForGlobalTrainingDataGrid = 0;
        private void globalTraingsDataGrid_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            ScrollViewer scrollViewer = e.OriginalSource as ScrollViewer;
            if (e.VerticalChange > 0)
            {
                if (scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                {
                    offsetForGlobalTrainingDataGrid++;
                    List<Training> trainings;
                    DateTime dateTime;
                    if (DateTime.TryParse(searchGlobalTreningTextBox.Text, out dateTime))
                    {
                        trainings = trainingDAO.GetTrainingsSeacrchByDate(dateTime, activeUser.Username, offsetForGlobalTrainingDataGrid * 30);
                    }
                    else
                        trainings = trainingDAO.GetTrainingsSeacrchByCreators(searchGlobalTreningTextBox.Text, activeUser.Username, offsetForGlobalTrainingDataGrid * 30);
                    if (trainings.Count == 0)
                    {
                        offsetForGlobalTrainingDataGrid--;
                        return;
                    }

                    trainings.ForEach(p => globalTraingsDataGrid.Items.Add(p));
                    globalTraingsDataGrid.ScrollIntoView(trainings[0]);



                }


            }
        }

        private void detailsGlobalTreningsButton_Click(object sender, RoutedEventArgs e)
        {
            if (globalTraingsDataGrid.SelectedIndex >= 0)
            {
                Window window = new DetailsMyTraining(globalTraingsDataGrid.SelectedItem as Training, activeUser, "global");
                window.Show();
            }
            else
            {
                new CustomMessageBox(Application.Current.Resources["detailsTraningMessage"].ToString(), Application.Current.Resources["selectTrainingErrorMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
            }
        }

        private void searchGlobalButton_Click(object sender, RoutedEventArgs e)
        {
            offsetForGlobalTrainingDataGrid = 0;
            globalTraingsDataGrid.Items.Clear();
           DateTime dateTime;
            if (DateTime.TryParse(searchGlobalTreningTextBox.Text, out dateTime))
            {
                trainingDAO.GetTrainingsSeacrchByDate(dateTime, activeUser.Username, offsetForGlobalTrainingDataGrid).ForEach(p => globalTraingsDataGrid.Items.Add(p));
            }
            else
                trainingDAO.GetTrainingsSeacrchByCreators(searchGlobalTreningTextBox.Text, activeUser.Username, offsetForGlobalTrainingDataGrid).ForEach(p => globalTraingsDataGrid.Items.Add(p));
        }

        private void searchGlobalTrening_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                searchGlobalButton_Click(null, null);
            }
        }

        private void saveManageAccountButton_Click(object sender, RoutedEventArgs e)
        {
            bool isAllSet = true;
            UserDAO userDAO = new UserDAO();
            if (!String.IsNullOrEmpty(nameTextBox.Text))
            {
                if (!activeUser.Name.Equals(nameTextBox.Text))
                {
                    if (userDAO.ChangeName(nameTextBox.Text, activeUser.Username))
                    {
                        activeUser.Name = nameTextBox.Text;

                    }
                    else
                    {
                        nameTextBox.Text = activeUser.Name;
                        isAllSet = false;
                    }
                }
            }
            if (!String.IsNullOrEmpty(surnameTextBox.Text))
            {
                if (!activeUser.Surname.Equals(surnameTextBox.Text))
                {
                    if (userDAO.ChangeSurname(surnameTextBox.Text, activeUser.Username))
                    {
                        activeUser.Surname = surnameTextBox.Text;

                    }
                    else
                    {
                        surnameTextBox.Text = activeUser.Surname;
                        isAllSet = false;
                    }
                }
            }
            if (!activeUser.Theme.Equals(themeComboBox.SelectedItem as String))
            {
                if (userDAO.ChangeTheme(themeComboBox.SelectedItem as String, activeUser.Username))
                {
                    activeUser.Theme = themeComboBox.SelectedItem as String;

                }
                else
                {
                    themeComboBox.SelectedItem = activeUser.Theme;
                    isAllSet = false;
                }
            }
            if (!activeUser.Language.Equals(languageComboBox.SelectedItem as String))
            {
                if (userDAO.ChangeLanguage(languageComboBox.SelectedItem as String, activeUser.Username))
                {
                    activeUser.Language = languageComboBox.SelectedItem as String;

                }
                else
                {
                    languageComboBox.SelectedItem = activeUser.Language;
                    isAllSet = false;
                }
            }
            if (!String.IsNullOrEmpty(passwordPasswordBox.Password))
            {
                if (passwordPasswordBox.Password.Equals(repeatPasswordPasswordBox.Password))
                {
                    if (!userDAO.ChangePassword(passwordPasswordBox.Password, activeUser.Username))
                    {
                        isAllSet = false;

                    }
                }
                passwordPasswordBox.Password = "";
                repeatPasswordPasswordBox.Password = "";

            }

            if (isAllSet)
            {
                new CustomMessageBox(Application.Current.Resources["allSuccessMessageInfo"].ToString(), Application.Current.Resources["allSuccessMessage"].ToString()).ShowDialog();
                SetThemeAndLanguage(activeUser);
            }
            else
            {
                //poruka da nije
            }
        }
        //sad za statistiku
        private String GetParseTime(int parseInt)
        {
            StringBuilder stringBuilder = new StringBuilder();
            int days = parseInt / 1440;
            if (days > 0)
                stringBuilder.Append(days + "d");
            parseInt -= days * 1440;
            int hours = parseInt / 60;
            if (hours > 0)
                stringBuilder.Append(" " + hours + "h");
            parseInt -= hours * 60;
            int minutes = parseInt;
            stringBuilder.Append(" " + minutes + "m");
            return stringBuilder.ToString();
        }

        private void CalculateStatistics()
        {
            int res = 0;
            for (int i = 1; i < 1_000_000_000; i++)
            {
                res++;
                res += 2 * 3;
            }
            Console.WriteLine(res);
            statistics=new StatisticsDAO().GetStatistics(activeUser.Username);
            
        }

        private void logoutButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            ResetTheme();
        }
    }
}
