﻿using Calisthenics_Trening_Network;
using CalisthenicsTreningNetworkHCI.DAO;
using CalisthenicsTreningNetworkHCI.DTO;
using MySqlX.XDevAPI.Common;
using Org.BouncyCastle.Crypto.Tls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CalisthenicsTreningNetworkHCI.GUI
{
    /// <summary>
    /// Interaction logic for AddExerciseAdmin.xaml
    /// </summary>
    public partial class AddChangeExerciseAdmin : Window
    {
        private Exercise exercise;
        private String type;
        private ExerciseDAO exerciseDAO = new ExerciseDAO();
        public AddChangeExerciseAdmin(Exercise exercise, String type)
        {
            this.exercise = exercise;
            this.type = type;
            InitializeComponent();
            SetCommponents();
            if ("Change".Equals(type))
                SetFields(exercise);
        }
        private void SetCommponents()
        {
            for(int i=1;i<=5;i++)
            levelExerciseComboBox.Items.Add($"{i}");
            typeExerciseComboBox.Items.Add("Pull");
            typeExerciseComboBox.Items.Add("Push");
        }
        private void SetFields(Exercise exercise)
        {
            nameExerciseTextBox.Text = exercise.Name;
            levelExerciseComboBox.SelectedItem = exercise.Level.ToString();
            typeExerciseComboBox.SelectedItem = exercise.Type;
            weightedExerciseTextBox.Text = exercise.Weighted.ToString();
            saveExerciseButton.Visibility = Visibility.Visible;
            addExerciseButton.Visibility = Visibility.Collapsed;
        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void addExerciseButton_Click(object sender, RoutedEventArgs e)
        {
            String name = nameExerciseTextBox.Text;
            String weightedString = weightedExerciseTextBox.Text;
            if(String.IsNullOrEmpty(name) || String.IsNullOrEmpty(weightedString) || levelExerciseComboBox.SelectedIndex <0 || typeExerciseComboBox.SelectedIndex < 0)
            {
                new CustomMessageBox(Application.Current.Resources["AddExerciseErrorMessage"].ToString(), Application.Current.Resources["fillFieldsMessage"].ToString()).ShowDialog();
            }
            else
            {
                if(Int32.TryParse(weightedString,out int weighted) && Int16.TryParse(levelExerciseComboBox.SelectedItem as String,out short level))
                {
                    exercise.Level = level;
                    exercise.Name = name;
                    exercise.Weighted = weighted;
                    exercise.Type = typeExerciseComboBox.SelectedItem as String;
                    int result = exerciseDAO.AddExercise(exercise);
                    if (result >= 0)
                    {
                        exercise.Id = result;
                        new CustomMessageBox(Application.Current.Resources["AddExerciseInfoMessage"].ToString(), Application.Current.Resources["AddExerciseMessage"].ToString()).ShowDialog();
                        DialogResult = true;
                        Close();
                    }
                    else new CustomMessageBox(Application.Current.Resources["AddExerciseErrorMessage"].ToString(), Application.Current.Resources["notAddExerciseMessage"].ToString()).ShowDialog();

                }
                else
                {
                    new CustomMessageBox(Application.Current.Resources["AddExerciseErrorMessage"].ToString(), Application.Current.Resources["validationInputErrorMessage"].ToString()).ShowDialog();
                }
            }
        }

        private void saveExerciseButton_Click(object sender, RoutedEventArgs e)
        {

            String name = nameExerciseTextBox.Text;
            String weightedString = weightedExerciseTextBox.Text;
            if (String.IsNullOrEmpty(name) || String.IsNullOrEmpty(weightedString) || levelExerciseComboBox.SelectedIndex < 0 || typeExerciseComboBox.SelectedIndex < 0)
            {
                new CustomMessageBox(Application.Current.Resources["changeExerciseErrorMessage"].ToString(), Application.Current.Resources["fillFieldsMessage"].ToString()).ShowDialog();
            }
            else
            {
                if (Int32.TryParse(weightedString, out int weighted) && Int16.TryParse(levelExerciseComboBox.SelectedItem as String, out short level))
                {
                    exercise.Level = level;
                    exercise.Name = name;
                    exercise.Weighted = weighted;
                    exercise.Type = typeExerciseComboBox.SelectedItem as String;
                    if (exerciseDAO.UpdateExercise(exercise))
                    {
                        new CustomMessageBox(Application.Current.Resources["changeExerciseInfoMessage"].ToString(), Application.Current.Resources["changeExerciseMessage"].ToString()).ShowDialog();
                        DialogResult = true;
                        Close();
                    }
                    else new CustomMessageBox(Application.Current.Resources["changeExerciseErrorMessage"].ToString(), Application.Current.Resources["notChangeExerciseMessage"].ToString()).ShowDialog();

                }
                else
                {
                    new CustomMessageBox(Application.Current.Resources["changeExerciseErrorMessage"].ToString(), Application.Current.Resources["validationInputErrorMessage"].ToString()).ShowDialog();
                }
            }
        }
    }
}
