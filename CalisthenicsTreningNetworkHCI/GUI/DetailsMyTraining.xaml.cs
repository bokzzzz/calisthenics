﻿using CalisthenicsTreningNetworkHCI.DAO;
using CalisthenicsTreningNetworkHCI.DTO;
using HCI_Calisthenics_Trening.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CalisthenicsTreningNetworkHCI.GUI
{
    /// <summary>
    /// Interaction logic for DetailsMyTraining.xaml
    /// </summary>
    public partial class DetailsMyTraining : Window
    {
        private User activeUser;
        private Training training;
        public DetailsMyTraining( Training training,User user,String mode)
        {
            activeUser = user;
            this.training = training;
            InitializeComponent();
            SetFields(training);
            if ("global".Equals(mode))
            {
                SetGlobal();
                
            }
        }

        private void SetGlobal()
        {
            ratingComboBox.Visibility = Visibility.Visible;
            for (int i = 1; i <= 10; i++)
                ratingComboBox.Items.Add(i);
            int res = new TrainingDAO().GetRateOnTeningByIdAndUsername(training.Id, activeUser.Username);
            if (res > 0)
                ratingComboBox.SelectedIndex = res - 1;
        }

        private void SetFields(Training training)
        {
            typeLocationLabel.Content += (" " + training.LocationType);
            spentTimeLabel.Content += (" " + training.SpentTime.ToString());
            dateTimeLabel.Content += (" " + training.DateTimeFormat);
            LocationDAO locationDAO = new LocationDAO();
            Location location = locationDAO.GetLocationByPostCodeAndStreet(new Location { PostCode = training.PostCode, Street = training.Street });
            locationLabel.Content += (" "+ location.Name+" " + location.Street +" " +location.PostCode);
            TrainingDAO trainingDAO = new TrainingDAO();
            ratingLabel.Content += (": " +training.Rating + " (" + trainingDAO.GetNumberOfVotesById(training.Id)+" "+ Application.Current.Resources["votesString"]+ " )");
            List<ExerciseOnTrening> exerciseOnTrenings = trainingDAO.GetExerciseOnTeningById(training.Id);
            foreach (ExerciseOnTrening exercise in exerciseOnTrenings)
                exerviseGrid.Items.Add(exercise);
            foreach (Comment comment in trainingDAO.GetCommentsOnTeningById(training.Id))
            {
                commentRichTextBox.AppendText(comment.DateTimeFormat + " " + comment.Creator + ": " + comment.Text+ Environment.NewLine);
            }
        }


        private void addCommentButoon_Click(object sender, RoutedEventArgs e)
        {
            String commentText = commentTextBox.Text;
            if (!String.IsNullOrEmpty(commentText))
            {
                Comment comment = new Comment
                {
                    Creator = activeUser.Username,
                    DateTime = DateTime.Now,
                    Id = training.Id,
                    Text = commentText
                };
                if (new CommentDAO().AddComment(comment))
                {
                    commentRichTextBox.AppendText(comment.DateTime.ToString("dd.MM.yyyy HH:mm") + " " + comment.Creator + ": " + comment.Text + Environment.NewLine);
                    commentTextBox.Text = "";
                    commentRichTextBox.CaretPosition = commentRichTextBox.Document.ContentStart;
                }
                else
                {
                    new CustomMessageBox(Application.Current.Resources["AddCommentError"].ToString(), Application.Current.Resources["cantAddComment"].ToString(), CustomMessageBoxIcon.Error);
                }
            }
        }

        private void ratingComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TrainingDAO trainingDAO = new TrainingDAO();
            if(trainingDAO.InsertOrUpdateRate(training.Id, activeUser.Username, (int)ratingComboBox.SelectedItem))
            {
                ratingLabel.Content =(ratingLabel.Content as String).Split(':')[0] +(": " + trainingDAO.GetRatingById(training.Id)+ " (" + trainingDAO.GetNumberOfVotesById(training.Id) + " " + Application.Current.Resources["votesString"] + " )");
            }
        }
    }
}
