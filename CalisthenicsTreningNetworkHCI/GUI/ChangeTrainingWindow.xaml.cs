﻿using CalisthenicsTreningNetworkHCI.DAO;
using CalisthenicsTreningNetworkHCI.DTO;
using HCI_Calisthenics_Trening.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CalisthenicsTreningNetworkHCI.GUI
{
    /// <summary>
    /// Interaction logic for ChangeTrainingWindow.xaml
    /// </summary>
    public partial class ChangeTrainingWindow : Window
    {
        private Training training;
        private User activeUser;
        private List<ExerciseOnTrening> exerciseOnTrenings;
        TrainingDAO trainingDAO = new TrainingDAO();
        ExerciseOnTreningDAO exerciseOnTreningDAO = new ExerciseOnTreningDAO();
        public ChangeTrainingWindow(Training training, User activeUser)
        {
            this.training = training;
            this.activeUser = activeUser;

            InitializeComponent();
            SetFields();
        }

        private void SetFields()
        {
            locationTypeTextBox.Text = training.LocationType;
            spentTimeTextBox.Text = training.SpentTime.ToString();
            dateTimePicker.Text = training.DateTime.ToString("dd.MM.yyyy HH:mm");
            exerciseOnTrenings = trainingDAO.GetExerciseOnTeningById(training.Id);
            var locations = new LocationDAO().GetLocations();
            foreach (Location location in locations)
                locationDataGrid.Items.Add(location);
            Location selectedLocation = locations.Find(p => p.PostCode.Equals(training.PostCode) && p.Street.Equals(training.Street));
            locationDataGrid.SelectedItem = selectedLocation;
            locationDataGrid.ScrollIntoView(selectedLocation);
            exerviseGrid.ItemsSource = exerciseOnTrenings;

        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        private void addExerciseButton_Click(object sender, RoutedEventArgs e)
        {
            ExerciseOnTrening exerciseOnTrening = new ExerciseOnTrening();
            Window window = new AddExerciseWindow(exerciseOnTrening, "Add");
            bool isSuccess = window.ShowDialog().Value;
            if (isSuccess)
            {
                bool isExist = exerciseOnTrenings.Exists(x => x.Id == exerciseOnTrening.Id && x.Reps == exerciseOnTrening.Reps);
                if (isExist)
                {
                    ExerciseOnTrening findExercise = exerciseOnTrenings.Find(x => x.Id == exerciseOnTrening.Id && x.Reps == exerciseOnTrening.Reps);
                    exerciseOnTreningDAO.RemoveExerciseOnTrening(findExercise, training.Id);
                    findExercise.Sets += exerciseOnTrening.Sets;
                    findExercise.Rest += exerciseOnTrening.Rest;
                    exerciseOnTreningDAO.AddExerciseOnTrening(findExercise, training.Id);
                    exerviseGrid.Items.Refresh();
                }
                else
                {
                    exerciseOnTrenings.Add(exerciseOnTrening);
                    exerciseOnTreningDAO.AddExerciseOnTrening(exerciseOnTrening, training.Id);
                    exerviseGrid.Items.Refresh();
                }
            }
        }

        private void removeExerciseButton_Click(object sender, RoutedEventArgs e)
        {
            if (exerviseGrid.SelectedIndex < 0)
            {
                new CustomMessageBox(Application.Current.Resources["RemoveExerciseTitleErrorMessage"].ToString(), Application.Current.Resources["chooseExerciseErrorMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
            }
            else
            {
                exerciseOnTrenings.Remove(exerviseGrid.SelectedItem as ExerciseOnTrening);
                exerciseOnTreningDAO.RemoveExerciseOnTrening(exerviseGrid.SelectedItem as ExerciseOnTrening, training.Id);
                exerviseGrid.Items.Refresh();
            }
        }

        private void ChangeExerciseButton_Click(object sender, RoutedEventArgs e)
        {
            if (exerviseGrid.SelectedIndex < 0)
            {
                new CustomMessageBox(Application.Current.Resources["ChangeExerciseTitleErrorMessage"].ToString(), Application.Current.Resources["chooseExerciseErrorMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
            }
            else
            {
                ExerciseOnTrening changeExercise = exerviseGrid.SelectedItem as ExerciseOnTrening;
                exerciseOnTrenings.Remove(changeExercise);
                exerciseOnTreningDAO.RemoveExerciseOnTrening(changeExercise, training.Id);
                Window window = new AddExerciseWindow(changeExercise, "Change");
                bool isSuccess = window.ShowDialog().Value;
                if (isSuccess)
                {
                    bool isExist = exerciseOnTrenings.Exists(x => x.Id == changeExercise.Id && x.Reps == changeExercise.Reps);
                    if (isExist)
                    {
                        ExerciseOnTrening findExercise = exerciseOnTrenings.Find(x => x.Id == changeExercise.Id && x.Reps == changeExercise.Reps);
                        exerciseOnTreningDAO.RemoveExerciseOnTrening(findExercise, training.Id);
                        findExercise.Sets += changeExercise.Sets;
                        findExercise.Rest += changeExercise.Rest;
                        exerciseOnTreningDAO.AddExerciseOnTrening(findExercise, training.Id);
                        exerviseGrid.Items.Refresh();
                    }
                    else
                    {
                        exerciseOnTrenings.Add(changeExercise);
                        exerciseOnTreningDAO.AddExerciseOnTrening(changeExercise, training.Id);
                        exerviseGrid.Items.Refresh();
                    }

                }
            }
        }

        private void addTreningButton_Click(object sender, RoutedEventArgs e)
        {
            String locationType = locationTypeTextBox.Text;
            String spentTimeString = spentTimeTextBox.Text;
            String dateTimeString = dateTimePicker.Text;
            if (String.IsNullOrEmpty(locationType) || String.IsNullOrEmpty(spentTimeString) || String.IsNullOrEmpty(dateTimeString))
            {
                new CustomMessageBox(Application.Current.Resources["addTrainingErrorMessage"].ToString(), Application.Current.Resources["fillFieldsMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }
            if (exerciseOnTrenings.Count < 1)
            {
                new CustomMessageBox(Application.Current.Resources["addTrainingErrorMessage"].ToString(), Application.Current.Resources["addExerciseErrorMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }
            if (locationDataGrid.SelectedIndex < 0)
            {
                new CustomMessageBox(Application.Current.Resources["addTrainingErrorMessage"].ToString(), Application.Current.Resources["addLocationErrorMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }
            DateTime dateTime;
            int spentTime;
            if (DateTime.TryParse(dateTimeString, out dateTime) && Int32.TryParse(spentTimeString, out spentTime))
            {
                Location location = locationDataGrid.SelectedItem as Location;
                //iymjena u bazi

                training.DateTime = dateTime;
                training.LocationType = locationType;
                training.Street = location.Street;
                training.PostCode = location.PostCode;
                training.SpentTime = spentTime;
              
                if (trainingDAO.UpadateTrening(training))
                {
                    new CustomMessageBox(Application.Current.Resources["changeTrainingInfoMessage"].ToString(), Application.Current.Resources["notChangedTrainingErrorMessage"].ToString(), CustomMessageBoxIcon.Information).ShowDialog() ;
                    DialogResult = true;
                    Close();
                    return;
                }
                else
                {
                  new CustomMessageBox(Application.Current.Resources["changeTrainingErrorMessage"].ToString(), Application.Current.Resources["notChangedTrainingErrorMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                }
            }
            else
            {
                new CustomMessageBox(Application.Current.Resources["changeTrainingErrorMessage"].ToString(), Application.Current.Resources["validationInputErrorMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }
        }
        private void ClearFields()
        {
            exerciseOnTrenings.Clear();
            exerviseGrid.Items.Refresh();
            locationTypeTextBox.Text = "";
            spentTimeTextBox.Text = "";
        }
    }
}
