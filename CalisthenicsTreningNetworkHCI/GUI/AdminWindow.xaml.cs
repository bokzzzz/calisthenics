﻿using Calisthenics_Trening_Network.DAO;
using CalisthenicsTreningNetworkHCI.DAO;
using CalisthenicsTreningNetworkHCI.DTO;
using CalisthenicsTreningNetworkHCI.GUI;
using HCI_Calisthenics_Trening.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Calisthenics_Trening_Network.GUI
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        private User admin;
        private ExerciseDAO exerciseDAO = new ExerciseDAO();
        private LocationDAO locationDAO = new LocationDAO();
        public AdminWindow(User user)
        {
            admin = user;
            InitializeComponent();
            SetThemeAndLanguage(admin);
            manageExercisesButton_Click(null, null);
        }

        private void manageExercisesButton_Click(object sender, RoutedEventArgs e)
        {
            addAdminGrid.Visibility = Visibility.Collapsed;
            manageAccountGrid.Visibility = Visibility.Collapsed;
            manageLocationGrid.Visibility = Visibility.Collapsed;
            manageExerciseGrid.Visibility = Visibility.Visible;
            var exercises = exerciseDAO.GetExercises();
            exercises.ForEach(ex => exerciseDataGrid.Items.Add(ex));
        }

        private void manageLocationButton_Click(object sender, RoutedEventArgs e)
        {
            manageExerciseGrid.Visibility = Visibility.Collapsed;
            addAdminGrid.Visibility = Visibility.Collapsed;
            manageAccountGrid.Visibility = Visibility.Collapsed;
            manageLocationGrid.Visibility = Visibility.Visible;
            locationDataGrid.Items.Clear();
            locationDAO.GetLocations().ForEach(x => locationDataGrid.Items.Add(x));
        }

        private void addAdminsButton_Click(object sender, RoutedEventArgs e)
        {
            manageExerciseGrid.Visibility = Visibility.Collapsed;
            manageAccountGrid.Visibility = Visibility.Collapsed;
            manageLocationGrid.Visibility = Visibility.Collapsed;
            addAdminGrid.Visibility = Visibility.Visible;
            SetFieldForAddAdmin();
        }

        private void manageAccountButton_Click(object sender, RoutedEventArgs e)
        {
            manageExerciseGrid.Visibility = Visibility.Collapsed;
            addAdminGrid.Visibility = Visibility.Collapsed;
            manageAccountGrid.Visibility = Visibility.Visible;
            manageLocationGrid.Visibility = Visibility.Collapsed;
            SetFieldForManageAccount();

        }
        private void logoutButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            DialogResult = true;
        }
        private void SetThemeAndLanguage(User user)
        {
            Application.Current.Resources.MergedDictionaries.Clear();
            SetTheme(user);
            SetLanguage(user);
        }
        private void SetTheme(User user)
        {
            if ("Dark theme".Equals(user.Theme))
            {
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("/Dict/Dark.xaml", UriKind.Relative) });
            }
            else if ("Classic theme".Equals(user.Theme))
            {
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("/Dict/Classic.xaml", UriKind.Relative) });
            }
            else
            {
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("/Dict/Blue.xaml", UriKind.Relative) });
            }
        }
        private void SetLanguage(User user)
        {
            if ("English".Equals(user.Language))
            {
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("/Dict/Eng.xaml", UriKind.Relative) });
            }
            else if ("Српски".Equals(user.Language))
            {
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("/Dict/SrbCir.xaml", UriKind.Relative) });
            }
            else
            {
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("/Dict/Srb.xaml", UriKind.Relative) });
            }
        }
        private void ResetTheme()
        {
            Application.Current.Resources.MergedDictionaries.Clear();
            Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri("/Dict/Classic.xaml", UriKind.Relative) });
        }

        private void deleteExerciseButton_Click(object sender, RoutedEventArgs e)
        {
            if (exerciseDataGrid.SelectedIndex >= 0)
            {
                if (exerciseDAO.DeleteExercise(exerciseDataGrid.SelectedItem as Exercise))
                {
                    new CustomMessageBox(Application.Current.Resources["deleteExerciseErrorMessage"].ToString(), Application.Current.Resources["deleteExerciseMessage"].ToString()).ShowDialog();
                    exerciseDataGrid.Items.RemoveAt(exerciseDataGrid.SelectedIndex);
                }
                else
                    new CustomMessageBox(Application.Current.Resources["deleteExerciseErrorMessage"].ToString(), Application.Current.Resources["notDeleteExerciseMessage"].ToString()).ShowDialog();
            }
            else
            {
                new CustomMessageBox(Application.Current.Resources["deleteExerciseErrorMessage"].ToString(), Application.Current.Resources["chooseExerciseErrorMessage"].ToString()).ShowDialog();
            }
        }

        private void changeExerciseButton_Click(object sender, RoutedEventArgs e)
        {
            if (exerciseDataGrid.SelectedIndex >= 0)
            {
                bool? isAdded = new AddChangeExerciseAdmin(exerciseDataGrid.SelectedItem as Exercise, "Change").ShowDialog();
                if (isAdded.HasValue && isAdded.Value)
                {
                    exerciseDataGrid.Items.Refresh();
                }
            }
            else
            {
                new CustomMessageBox(Application.Current.Resources["changeExerciseErrorMessage"].ToString(), Application.Current.Resources["chooseExerciseErrorMessage"].ToString()).ShowDialog();
            }
        }

        private void addExerciseButton_Click(object sender, RoutedEventArgs e)
        {
            Exercise exercise = new Exercise();
            bool? isAdded = new AddChangeExerciseAdmin(exercise, "Add").ShowDialog();
            if (isAdded.HasValue && isAdded.Value)
            {
                exerciseDataGrid.Items.Add(exercise);
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            ResetTheme();
        }
        private void SetFieldForAddAdmin()
        {
            if (themeComboBox.Items.Count < 1)
            {
                themeComboBox.Items.Add("Dark theme");
                themeComboBox.Items.Add("Dark blue theme");
                themeComboBox.Items.Add("Classic theme");
            }
            if (languageComboBox.Items.Count < 1)
            {
                languageComboBox.Items.Add("English");
                languageComboBox.Items.Add("Srpski");
                languageComboBox.Items.Add("Српски");
            }
        }

        private void addAdminButton_Click(object sender, RoutedEventArgs e)
        {
            String username = usernameTextBox.Text;
            String name = nameTextBox.Text;
            String surname = surnameTextBox.Text;
            String theme = themeComboBox.SelectedItem != null ? themeComboBox.SelectedItem.ToString() : null;
            String language = languageComboBox.SelectedItem != null ? languageComboBox.SelectedItem.ToString() : null;
            String password = passwordPasswordBox.Password;
            String repeatPassword = repeatPasswordPasswordBox.Password;
            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(name) || String.IsNullOrEmpty(surname) || String.IsNullOrEmpty(theme)
                || String.IsNullOrEmpty(language) || String.IsNullOrEmpty(password) || String.IsNullOrEmpty(repeatPassword))
            {
                new CustomMessageBox(Application.Current.Resources["addAdminError"].ToString(), Application.Current.Resources["fillFieldsMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }
            if (password.Equals(repeatPassword))
            {
                UserDAO userDAO = new UserDAO();
                User user = new User { Username = username };
                int result = userDAO.isUsernameExist(user);
                if (result == 0)
                {
                    user.Name = name;
                    user.Surname = surname;
                    user.Theme = theme;
                    user.Language = language;
                    user.Password = password;
                    if (userDAO.AddAdmin(user))
                    {
                        new CustomMessageBox(Application.Current.Resources["addAdminInfo"].ToString(), Application.Current.Resources["accountSuccessfullyAddedMessage"].ToString()).ShowDialog();
                        ResetFields();
                        return;
                    }
                    else
                    {
                        new CustomMessageBox(Application.Current.Resources["addAdminError"].ToString(), Application.Current.Resources["accountNotSuccessfullyAddedMessage"].ToString(), CustomMessageBoxIcon.Error).ToString();
                    }
                }
                else if (result == 1)
                {
                    new CustomMessageBox(Application.Current.Resources["addAdminError"].ToString(), Application.Current.Resources["usernameAlreadyExistMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                    return;
                }
                else
                {
                    new CustomMessageBox(Application.Current.Resources["addAdminError"].ToString(), Application.Current.Resources["signupLaterMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                }
            }
            else
            {
                new CustomMessageBox(Application.Current.Resources["addAdminError"].ToString(), Application.Current.Resources["samePasswordMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }
        }
        private void ResetFields()
        {
            usernameTextBox.Text = "";
            nameTextBox.Text = "";
            surnameTextBox.Text = "";
            passwordPasswordBox.Password = "";
            repeatPasswordPasswordBox.Password = "";
            themeComboBox.SelectedIndex = 0;
            languageComboBox.SelectedIndex = 0;

        }
        private void saveManageAccountButton_Click(object sender, RoutedEventArgs e)
        {
            bool isAllSet = true;
            bool isThemeChange = false;
            UserDAO userDAO = new UserDAO();
            if (!String.IsNullOrEmpty(passwordPasswordBoxAcc.Password))
            {
                if (passwordPasswordBoxAcc.Password.Equals(repeatPasswordPasswordBoxAcc.Password))
                {
                    if (!userDAO.ChangePassword(passwordPasswordBoxAcc.Password, admin.Username))
                    {
                        isAllSet = false;

                    }
                }
                else
                {
                    new CustomMessageBox(Application.Current.Resources["addAdminError"].ToString(), Application.Current.Resources["samePasswordMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                    isAllSet = false;
                    return;
                }
                passwordPasswordBoxAcc.Password = "";
                repeatPasswordPasswordBoxAcc.Password = "";

            }
            if (!String.IsNullOrEmpty(nameTextBoxAcc.Text))
            {
                if (!admin.Name.Equals(nameTextBox.Text))
                {
                    if (userDAO.ChangeName(nameTextBoxAcc.Text, admin.Username))
                    {
                        admin.Name = nameTextBox.Text;

                    }
                    else
                    {
                        nameTextBoxAcc.Text = admin.Name;
                        isAllSet = false;
                    }
                }
            }
            if (!String.IsNullOrEmpty(surnameTextBoxAcc.Text))
            {
                if (!admin.Surname.Equals(surnameTextBoxAcc.Text))
                {
                    if (userDAO.ChangeSurname(surnameTextBoxAcc.Text, admin.Username))
                    {
                        admin.Surname = surnameTextBoxAcc.Text;

                    }
                    else
                    {
                        surnameTextBoxAcc.Text = admin.Surname;
                        isAllSet = false;
                    }
                }
            }
            if (!admin.Theme.Equals(themeComboBoxAcc.SelectedItem as String))
            {
                if (userDAO.ChangeTheme(themeComboBoxAcc.SelectedItem as String, admin.Username))
                {
                    admin.Theme = themeComboBoxAcc.SelectedItem as String;
                    isThemeChange = true;
                }
                else
                {
                    themeComboBoxAcc.SelectedItem = admin.Theme;
                    isAllSet = false;
                }
            }
            if (!admin.Language.Equals(languageComboBoxAcc.SelectedItem as String))
            {
                if (userDAO.ChangeLanguage(languageComboBoxAcc.SelectedItem as String, admin.Username))
                {
                    admin.Language = languageComboBoxAcc.SelectedItem as String;
                    isThemeChange = true;
                }
                else
                {
                    languageComboBoxAcc.SelectedItem = admin.Language;
                    isAllSet = false;
                }
            }


            if (isAllSet)
            {
                new CustomMessageBox(Application.Current.Resources["allSuccessMessageInfo"].ToString(), Application.Current.Resources["allSuccessMessage"].ToString()).ShowDialog();
                if (isThemeChange)
                    SetThemeAndLanguage(admin);
            }
            else
            {
                new CustomMessageBox(Application.Current.Resources["allNotSuccessMessageInfo"].ToString(), Application.Current.Resources["allNotSuccessMessage"].ToString()).ShowDialog();
            }
        }
        private void SetFieldForManageAccount()
        {
            usernameTextBoxAcc.Text = admin.Username;
            usernameTextBoxAcc.IsReadOnly = true;
            nameTextBoxAcc.Text = admin.Name;
            surnameTextBoxAcc.Text = admin.Surname;
            if (themeComboBoxAcc.Items.Count < 1)
            {
                themeComboBoxAcc.Items.Add("Dark theme");
                themeComboBoxAcc.Items.Add("Dark blue theme");
                themeComboBoxAcc.Items.Add("Classic theme");
            }
            if (languageComboBoxAcc.Items.Count < 1)
            {
                languageComboBoxAcc.Items.Add("English");
                languageComboBoxAcc.Items.Add("Srpski");
                languageComboBoxAcc.Items.Add("Српски");
            }
            themeComboBoxAcc.SelectedItem = admin.Theme;
            languageComboBoxAcc.SelectedItem = admin.Language;
            passwordPasswordBoxAcc.Password = "";
            repeatPasswordPasswordBoxAcc.Password = "";
        }

        private void addLocationButton_Click(object sender, RoutedEventArgs e)
        {
            Location location = new Location();
            bool? isChanged = new AddChangeLocation(location, "Add").ShowDialog();
            if (isChanged.HasValue && isChanged.Value)
            {
                locationDataGrid.Items.Add(location);
            }

        }

        private void changeLocationButton_Click(object sender, RoutedEventArgs e)
        {
            if (locationDataGrid.SelectedIndex >= 0)
            {
                bool? isChanged = new AddChangeLocation(locationDataGrid.SelectedItem as Location, "Change").ShowDialog();
                if (isChanged.HasValue && isChanged.Value)
                {
                    locationDataGrid.Items.Refresh();
                }
            }
            else
            {
                new CustomMessageBox(Application.Current.Resources["changeLocationError"].ToString(), Application.Current.Resources["addLocationErrorMessage"].ToString(),CustomMessageBoxIcon.Error).ShowDialog();
            }
        }

        private void deleteLocationButton_Click(object sender, RoutedEventArgs e)
        {
            if (locationDataGrid.SelectedIndex >= 0)
            {
                if (locationDAO.DeleteLocation(locationDataGrid.SelectedItem as Location))
                {
                    new CustomMessageBox(Application.Current.Resources["deleteLocationInfo"].ToString(), Application.Current.Resources["deleteLocationInfoMessage"].ToString()).ShowDialog();
                    locationDataGrid.Items.Remove(locationDataGrid.SelectedItem as Location);
                }
                else
                {
                    new CustomMessageBox(Application.Current.Resources["deleteLocationError"].ToString(), Application.Current.Resources["notDeleteLocationInfoMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                }
            }
            else
            {
                new CustomMessageBox(Application.Current.Resources["deleteLocationError"].ToString(), Application.Current.Resources["addLocationErrorMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
            }
        }
    }
}
