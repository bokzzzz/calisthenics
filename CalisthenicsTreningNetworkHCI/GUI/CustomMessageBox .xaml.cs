﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CalisthenicsTreningNetworkHCI.GUI
{
    /// <summary>
    /// Interaction logic for CustomMessageBox.xaml
    /// </summary>
    public partial class CustomMessageBox : Window
    {
        public CustomMessageBox(string title, string text, CustomMessageBoxIcon icon = CustomMessageBoxIcon.Information)
        {
            InitializeComponent();
            this.ShowInTaskbar = false;
            titleBlock.Text = text;
            Title = title;


            //Set icon
            if (icon.Equals(CustomMessageBoxIcon.Warning))
                packIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Warning;
            else if (icon.Equals(CustomMessageBoxIcon.Error))
                packIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Error;
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }

}

