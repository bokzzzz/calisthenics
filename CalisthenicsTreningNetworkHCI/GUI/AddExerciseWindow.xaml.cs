﻿using Calisthenics_Trening_Network;
using CalisthenicsTreningNetworkHCI.DAO;
using CalisthenicsTreningNetworkHCI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CalisthenicsTreningNetworkHCI.GUI
{
    /// <summary>
    /// Interaction logic for AddExerciseWindow.xaml
    /// </summary>
    public partial class AddExerciseWindow : Window
    {
        private ExerciseOnTrening exerciseOnTreningTmp;

        public AddExerciseWindow(ExerciseOnTrening exerciseOnTrening,String type)
        {
            InitializeComponent();
            SetExercise();
            exerciseOnTreningTmp = exerciseOnTrening;
            if ("Change".Equals(type))
                SetChangeParameter(exerciseOnTrening);
        }

        private void SetChangeParameter(ExerciseOnTrening exercise)
        {
            addExerciseOnTreningButton.SetResourceReference(ContentProperty, "saveContentButton");
            onTopLabel.SetResourceReference(ContentProperty, "changeExerciseOnTreningLabel");
            repsTextBox.Text = exercise.Reps.ToString();
            setsTextBox.Text = exercise.Sets.ToString();
            restTextBox.Text = exercise.Rest.ToString();
            exerciseDataGrid.SelectedIndex = exerciseDataGrid.Items.Cast<Exercise>().ToList().FindIndex(e => e.Id == exercise.Id);
        }

        private void SetExercise()
        {
            foreach (Exercise exercise in new ExerciseDAO().GetExercises())
                exerciseDataGrid.Items.Add(exercise);
        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void addExerciseOnTreningButton_Click(object sender, RoutedEventArgs e)
        {
            String setsString = setsTextBox.Text;
            String repsString = repsTextBox.Text;
            String restString = restTextBox.Text;
            if(String.IsNullOrEmpty(setsString) || String.IsNullOrEmpty(repsString) || String.IsNullOrEmpty(restString) 
                || exerciseDataGrid.SelectedIndex < 0)
            {
               new CustomMessageBox(Application.Current.Resources["addExerciseOnTrainingErrorMessage"].ToString(),Application.Current.Resources["fillFieldsMessage"].ToString(),  CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }
            int rest, reps;
            short sets;
            if(Int32.TryParse(repsString, out reps) && Int32.TryParse(restString,out rest) && Int16.TryParse(setsString,out sets))
            {
                Exercise exercise = exerciseDataGrid.SelectedItem as Exercise;
                exerciseOnTreningTmp.Reps = reps;
                exerciseOnTreningTmp.Sets = sets;
                exerciseOnTreningTmp.Rest = rest;
                exerciseOnTreningTmp.Id = exercise.Id;
                exerciseOnTreningTmp.Level = exercise.Level;
                exerciseOnTreningTmp.Name = exercise.Name;
                exerciseOnTreningTmp.Type = exercise.Type;
                exerciseOnTreningTmp.Weighted = exercise.Weighted;
                new CustomMessageBox(Application.Current.Resources["addExerciseOnTrainingInfo"].ToString(), Application.Current.Resources["addExerciseOnTrainingInfoMessage"].ToString(), CustomMessageBoxIcon.Information).ShowDialog() ;
                DialogResult = true;
                Close();

            }
            else
            {
                new CustomMessageBox(Application.Current.Resources["addExerciseOnTrainingErrorMessage"].ToString(),Application.Current.Resources["validationInputErrorMessage"].ToString(), CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }
        }
    }
}
