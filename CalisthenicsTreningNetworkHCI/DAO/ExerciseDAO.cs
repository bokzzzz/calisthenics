﻿using Calisthenics_Trening_Network.DB;
using CalisthenicsTreningNetworkHCI.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalisthenicsTreningNetworkHCI.DAO
{
    class ExerciseDAO
    {
        public List<Exercise> GetExercises()
        {
            List<Exercise> exercises = new List<Exercise>();
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select * from vjezba where JeAktivna=1";
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    exercises.Add(new Exercise
                    {
                        Id=reader.GetInt32("IdVjezbe"),
                        Name=reader.GetString("Naziv"),
                        Weighted=reader.GetInt32("Opterecenje"),
                        Level=reader.GetInt16("TezinaIzvodjenja"),
                        Type=reader.GetString("TipVjezbe")
                       
                    });
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return exercises;
        }
        public Dictionary<int, int> GetExercisesID()
        {
            Dictionary<int,int> exercises = new Dictionary<int, int>();
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select IdVjezbe from vjezba";
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    exercises.Add(reader.GetInt32("IdVjezbe"), 0);
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return exercises;
        }
        public Exercise GetExerciseById(int id)
        {
            Exercise exercise = null;
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select * from vjezba where IdVjezbe=@id";
            mySqlCommand.Parameters.AddWithValue("@id",id);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    exercise=new Exercise
                    {
                        Name = reader.GetString("Naziv"),
                        Weighted = reader.GetInt32("Opterecenje"),
                        Level = reader.GetInt16("TezinaIzvodjenja"),
                        Type = reader.GetString("TipVjezbe")

                    };
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return exercise;
        }
        public int  AddExercise(Exercise exercise)
        {
            int result = -1;
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "insert into vjezba(Naziv, Opterecenje, TezinaIzvodjenja, TipVjezbe) values(@name, @weighted, @level, @type) ";
            mySqlCommand.Parameters.AddWithValue("@name", exercise.Name);
            mySqlCommand.Parameters.AddWithValue("@weighted", exercise.Weighted);
            mySqlCommand.Parameters.AddWithValue("@level", exercise.Level);
            mySqlCommand.Parameters.AddWithValue("@type", exercise.Type);
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                result =(int) mySqlCommand.LastInsertedId;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return result;
        }
        public bool UpdateExercise(Exercise exercise)
        {
            bool result = false;
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "update vjezba set Naziv = @name, Opterecenje = @weighted, TezinaIzvodjenja = @level, TipVjezbe =  @type  where IdVjezbe = @id ;";
            mySqlCommand.Parameters.AddWithValue("@name", exercise.Name);
            mySqlCommand.Parameters.AddWithValue("@weighted", exercise.Weighted);
            mySqlCommand.Parameters.AddWithValue("@level", exercise.Level);
            mySqlCommand.Parameters.AddWithValue("@type", exercise.Type);
            mySqlCommand.Parameters.AddWithValue("@id", exercise.Id);
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                result = true;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
            }
            connection.Close();
            return result;
        }
        public bool DeleteExercise(Exercise exercise)
        {
            bool result = false;
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "update vjezba set JeAktivna=0  where IdVjezbe = @id ;";
            mySqlCommand.Parameters.AddWithValue("@id", exercise.Id);
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                result = true;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
            }
            connection.Close();
            return result;
        }
    }
}

