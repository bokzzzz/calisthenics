﻿using Calisthenics_Trening_Network.DB;
using CalisthenicsTreningNetworkHCI.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalisthenicsTreningNetworkHCI.DAO
{
    class LocationDAO
    {
        public List<Location> GetLocations()
        {
            List<Location> locations = new List<Location>();
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select * from mjesto where JeAktivno=1";
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    locations.Add( new Location
                    {
                        
                        PostCode=reader.GetInt32("PostanskiBroj"),
                        Name=reader.GetString("Naziv"),
                        Street=reader.GetString("Ulica")
                    });
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return locations;
        }
        public Location GetLocationByPostCodeAndStreet(Location location)
        {
            Location locationRet=null;
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select * from mjesto where PostanskiBroj=@postCode and Ulica=@street";
            mySqlCommand.Parameters.AddWithValue("postCode",location.PostCode);
            mySqlCommand.Parameters.AddWithValue("street",location.Street);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    locationRet=new Location
                    {

                        PostCode = reader.GetInt32("PostanskiBroj"),
                        Name = reader.GetString("Naziv"),
                        Street = reader.GetString("Ulica")
                    };
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return locationRet;
        }
        public bool DeleteLocation(Location location)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "update mjesto set JeAktivno=0 where PostanskiBroj=@postCode and Ulica=@street";
            mySqlCommand.Parameters.AddWithValue("@postCode", location.PostCode);
            mySqlCommand.Parameters.AddWithValue("@street", location.Street);
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (MySqlException)
            {
                return false;
            }
        }
        public bool AddLocation(Location location)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "insert into mjesto(PostanskiBroj,Ulica,Naziv) values (@postCode,@street,@name)";
            mySqlCommand.Parameters.AddWithValue("@postCode", location.PostCode);
            mySqlCommand.Parameters.AddWithValue("@street", location.Street);
            mySqlCommand.Parameters.AddWithValue("@name", location.Name);
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (MySqlException)
            {
                return false;
            }
        }
        public int IsExist(Location location)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            int result = 0;
            mySqlCommand.CommandText = "select count(*) as broj from mjesto where PostanskiBroj=@postCode and Ulica=@street";
            mySqlCommand.Parameters.AddWithValue("@postCode", location.PostCode);
            mySqlCommand.Parameters.AddWithValue("@street", location.Street);
            mySqlCommand.Prepare();
            try
            {
                var reader = mySqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    result = reader.GetInt32("broj");
                }
                connection.Close();
                return result;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.Message);
                return -1;
            }
        }
        public bool ChangeLocation(Location location,Location oldLocation)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "update mjesto set PostanskiBroj=@postCode , Naziv=@name , Ulica= @street where PostanskiBroj = @postCodee and Ulica = @streett ; ";
            mySqlCommand.Parameters.AddWithValue("@postCode", location.PostCode);
            mySqlCommand.Parameters.AddWithValue("@street", location.Street);
            mySqlCommand.Parameters.AddWithValue("@name", location.Name);
            mySqlCommand.Parameters.AddWithValue("@postCodee", oldLocation.PostCode);
            mySqlCommand.Parameters.AddWithValue("@streett", oldLocation.Street);
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public bool ChangeName(Location location)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "update mjesto set  Naziv=@name  where PostanskiBroj = @postCodee and Ulica = @streett ; ";
            mySqlCommand.Parameters.AddWithValue("@name", location.Name);
            mySqlCommand.Parameters.AddWithValue("@postCodee", location.PostCode);
            mySqlCommand.Parameters.AddWithValue("@streett", location.Street);
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
