﻿using CalisthenicsTreningNetworkHCI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalisthenicsTreningNetworkHCI.DAO
{
    class StatisticsDAO
    {
        public Statistics GetStatistics(String username)
        {
            Statistics statistics = new Statistics();
            TrainingDAO trainingDAO = new TrainingDAO();
            //number of trenings
            statistics.ThisMonthTrenings= trainingDAO.GetNumberOfTreningThisMonth(username);
            statistics.PreviouseMonthTrenings= trainingDAO.GetNumberOfTreningPreviousMonth(username);
            statistics.ThisYearTrenings= trainingDAO.GetNumberOfTreningThisYear(username);
            statistics.PreviouseYearTrenings = trainingDAO.GetNumberOfTreningPreviousYear(username);
            //spent time
            statistics.ThisMonthSpentTime = trainingDAO.GetSpentTimeThisMonth(username);
            statistics.PreviouseMonthSpentTime = trainingDAO.GetSpentTimePreviousMonth(username);
            statistics.ThisYearSpentTime = trainingDAO.GetSpentTimeThisYear(username);
            statistics.PreviouseYearSpentTime = trainingDAO.GetSpentTimePreviousYear(username);
            //sets and reps
            Tuple<int, int> setsAndRepsThisMonth = trainingDAO.GetNumberOfSetsAndRepsThisMonth(username);
            statistics.ThisMonthReps = setsAndRepsThisMonth.Item1;
            statistics.ThisMonthSets = setsAndRepsThisMonth.Item2;
            Tuple<int, int> setsAndRepsThisYear = trainingDAO.GetNumberOfSetsAndRepsThisYear(username);
            statistics.ThisYearReps = setsAndRepsThisYear.Item1;
            statistics.ThisYearSets = setsAndRepsThisYear.Item2;
            Tuple<int, int> setsAndRepsPreviousYear = trainingDAO.GetNumberOfSetsAndRepsPreviousYear(username);
            statistics.PreviouseYearReps = setsAndRepsPreviousYear.Item1;
            statistics.PreviouseYearSets = setsAndRepsPreviousYear.Item2;
            Tuple<int, int> setsAndRepsPreviousMonth = trainingDAO.GetNumberOfSetsAndRepsPreviousMonth(username);
            statistics.PreviouseMonthReps = setsAndRepsPreviousMonth.Item1;
            statistics.PreviouseMonthSets = setsAndRepsPreviousMonth.Item2;
            //best record
            statistics.ExerciseOnTrenings = trainingDAO.GetPersonalBestRecordAllExercise(username);
            return statistics;
        }
    }
}
