﻿using Calisthenics_Trening_Network.DB;
using CalisthenicsTreningNetworkHCI.DTO;
using HCI_Calisthenics_Trening.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CalisthenicsTreningNetworkHCI.DAO
{
    class TrainingDAO
    {
        public bool AddTrening(Training trening)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "Insert into trening(Trajanje, DanIVrijemeOdrzavanja, TipMjesta, Mjesto_PostanskiBroj, Mjesto_Ulica, Korisnik_KorisnickoIme) values(" +
                "@spentTime, @dateTime, @locationType, @postCode, @street, @creator) ;";
            mySqlCommand.Parameters.AddWithValue("@spentTime", trening.SpentTime);
            mySqlCommand.Parameters.AddWithValue("@dateTime", trening.DateTime);
            mySqlCommand.Parameters.AddWithValue("@locationType", trening.LocationType);
            mySqlCommand.Parameters.AddWithValue("@postCode", trening.PostCode);
            mySqlCommand.Parameters.AddWithValue("@street", trening.Street);
            mySqlCommand.Parameters.AddWithValue("@creator", trening.Creator);
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                trening.Id = (int)mySqlCommand.LastInsertedId;
                ExerciseOnTreningDAO exerciseOnTreningDAO = new ExerciseOnTreningDAO();
                foreach (ExerciseOnTrening exercise in trening.Exercises)
                {
                    if (!exerciseOnTreningDAO.AddExerciseOnTrening(exercise, trening.Id)) return false;
                }
                return true;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return false;
        }
        public List<Training> GetTrainings(User user, int offset)
        {
            List<Training> trainings = new List<Training>();
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select * from trening where Korisnik_KorisnickoIme=@username and JeAktivan=1 order by DanIVrijemeOdrzavanja desc limit 30 offset @offset";
            mySqlCommand.Parameters.AddWithValue("@username", user.Username);
            mySqlCommand.Parameters.AddWithValue("@offset", offset);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    trainings.Add(new Training
                    {
                        Id = reader.GetInt32("IdTreninga"),
                        DateTime = reader.GetDateTime("DanIVrijemeOdrzavanja"),
                        LocationType = reader.GetString("TipMjesta"),
                        SpentTime = reader.GetInt32("Trajanje"),
                        PostCode = reader.GetInt32("Mjesto_PostanskiBroj"),
                        Street = reader.GetString("Mjesto_Ulica")

                    }); ;
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return trainings;
        }
        public String GetRatingById(int id)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "SELECT avg(Ocjena) as prosjek FROM calisthenics.ocjenjujetrening where Trening_IdTreninga=@id;";
            mySqlCommand.Parameters.AddWithValue("@id", id);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    String prosjek = reader.IsDBNull(0) ? "N/A" : reader.GetDouble("prosjek").ToString();
                    return prosjek;
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return "N/A";
        }
        public String GetNumberOfVotesById(int id)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "SELECT count(Ocjena) as prosjek FROM calisthenics.ocjenjujetrening where Trening_IdTreninga=@id;";
            mySqlCommand.Parameters.AddWithValue("@id", id);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    String prosjek = reader.IsDBNull(0) ? "0" : reader.GetDouble("prosjek").ToString();
                    return prosjek;
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return "0";
        }
        public List<ExerciseOnTrening> GetExerciseOnTeningById(int id)
        {
            List<ExerciseOnTrening> exercises = new List<ExerciseOnTrening>();
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select * from vjezbanatreningView where Trening_IdTreninga=@id;";
            mySqlCommand.Parameters.AddWithValue("@id", id);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    exercises.Add(new ExerciseOnTrening
                    {
                        Id = reader.GetInt32("IdVjezbe"),
                        Name = reader.GetString("Naziv"),
                        Weighted = reader.GetInt32("Opterecenje"),
                        Level = reader.GetInt16("TezinaIzvodjenja"),
                        Type = reader.GetString("TipVjezbe"),
                        Reps = reader.GetInt32("BrojPonavljanja"),
                        Rest = reader.GetInt32("OdmorIzmedjuSetova"),
                        Sets = reader.GetInt16("Setovi")
                    });
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return exercises;
        }
        public List<Comment> GetCommentsOnTeningById(int id)
        {
            List<Comment> comments = new List<Comment>();
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select * from komentarisetrening where Trening_IdTreninga=@id order by Vrijeme asc;";
            mySqlCommand.Parameters.AddWithValue("@id", id);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    comments.Add(new Comment
                    {
                        Text = reader.GetString("Tekst"),
                        DateTime = reader.GetDateTime("Vrijeme"),
                        Creator = reader.GetString("Korisnik_KorisnickoIme")
                    });
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return comments;
        }
        public bool removeTrening(Training training)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "update trening set JeAktivan=0 where IdTreninga=@id;";
            mySqlCommand.Parameters.AddWithValue("@id", training.Id);
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                return true;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return false;
        }
        public List<Training> GetGlobalTrainings(User user, int offset)
        {
            List<Training> trainings = new List<Training>();
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select * from trening where Korisnik_KorisnickoIme <> @username and JeAktivan=1 order by DanIVrijemeOdrzavanja desc limit 30 offset @offset";
            mySqlCommand.Parameters.AddWithValue("@username", user.Username);
            mySqlCommand.Parameters.AddWithValue("@offset", offset);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    trainings.Add(new Training
                    {
                        Id = reader.GetInt32("IdTreninga"),
                        DateTime = reader.GetDateTime("DanIVrijemeOdrzavanja"),
                        LocationType = reader.GetString("TipMjesta"),
                        SpentTime = reader.GetInt32("Trajanje"),
                        Rating = GetRatingById(reader.GetInt32("IdTreninga")),
                        PostCode = reader.GetInt32("Mjesto_PostanskiBroj"),
                        Street = reader.GetString("Mjesto_Ulica"),
                        Creator = reader.GetString("Korisnik_KorisnickoIme")

                    }); ;
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return trainings;
        }
        public int GetRateOnTeningByIdAndUsername(int id,String username)
        {
            int res = 0;
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "SELECT Ocjena FROM calisthenics.ocjenjujetrening where Trening_IdTreninga=@id and Korisnik_KorisnickoIme=@username;";
            mySqlCommand.Parameters.AddWithValue("@id", id);
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    res = reader.GetInt32("Ocjena");
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return res;
        }
        public bool InsertOrUpdateRate(int id,String username,int rate)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "insert into ocjenjujetrening (Ocjena,Trening_IdTreninga,Korisnik_KorisnickoIme) values(@rate,@id,@username)" +
                " on duplicate key update Ocjena=@rate";
            mySqlCommand.Parameters.AddWithValue("@id", id);
            mySqlCommand.Parameters.AddWithValue("@rate", rate);
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                return true;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return false;
        }
        public List<Training> GetTrainingsSeacrchByCreators(String pattern,String username,int offset)
        {
            List<Training> trainings = new List<Training>();
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select * from trening where Korisnik_KorisnickoIme <> @username and JeAktivan=1 and Korisnik_KorisnickoIme like @patern order by DanIVrijemeOdrzavanja desc limit 30 offset @offset";
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Parameters.AddWithValue("@offset", offset);
            mySqlCommand.Parameters.AddWithValue("@patern", new Regex("%" + pattern + "%"));
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    trainings.Add(new Training
                    {
                        Id = reader.GetInt32("IdTreninga"),
                        DateTime = reader.GetDateTime("DanIVrijemeOdrzavanja"),
                        LocationType = reader.GetString("TipMjesta"),
                        SpentTime = reader.GetInt32("Trajanje"),
                        PostCode = reader.GetInt32("Mjesto_PostanskiBroj"),
                        Street = reader.GetString("Mjesto_Ulica"),
                        Creator=reader.GetString("Korisnik_KorisnickoIme")
                    }); ;
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.Message);
            }
            connection.Close();
            return trainings;
        }
        public List<Training> GetTrainingsSeacrchByDate(DateTime dateTime, String username, int offset)
        {
            List<Training> trainings = new List<Training>();
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select * from trening where Korisnik_KorisnickoIme <> @username and JeAktivan=1  and date(DanIVrijemeOdrzavanja)=date(@date) order by DanIVrijemeOdrzavanja desc limit 30 offset @offset";
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Parameters.AddWithValue("@offset", offset);
            mySqlCommand.Parameters.AddWithValue("@date", dateTime.ToString("yyyy-MM-dd"));
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    trainings.Add(new Training
                    {
                        Id = reader.GetInt32("IdTreninga"),
                        DateTime = reader.GetDateTime("DanIVrijemeOdrzavanja"),
                        LocationType = reader.GetString("TipMjesta"),
                        SpentTime = reader.GetInt32("Trajanje"),
                        PostCode = reader.GetInt32("Mjesto_PostanskiBroj"),
                        Street = reader.GetString("Mjesto_Ulica"),
                        Creator = reader.GetString("Korisnik_KorisnickoIme")
                    }); ;
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return trainings;
        }
        public int GetNumberOfTreningThisMonth(String username)
        {
            int result = -1;
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select count(IdTreninga) as broj from trening where year(DanIVrijemeOdrzavanja) = @year and month(DanIVrijemeOdrzavanja)=@month and Korisnik_KorisnickoIme=@username and JeAktivan=1 ;";
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Parameters.AddWithValue("@year", DateTime.Now.Year);
            mySqlCommand.Parameters.AddWithValue("@month", DateTime.Now.Month);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    result = reader.GetInt32("broj");
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return result;
        }
        public int GetNumberOfTreningPreviousMonth(String username)
        {
            int result = -1;
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            DateTime dateTime = DateTime.Now;
            dateTime = dateTime.AddMonths(-1);
            mySqlCommand.CommandText = "select count(IdTreninga) as broj from trening where year(DanIVrijemeOdrzavanja) = @year and month(DanIVrijemeOdrzavanja)=@month and Korisnik_KorisnickoIme=@username and JeAktivan=1 ;";
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Parameters.AddWithValue("@year", dateTime.Year);
            mySqlCommand.Parameters.AddWithValue("@month", dateTime.Month);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    result = reader.GetInt32("broj");
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return result;
        }
        public int GetNumberOfTreningThisYear(String username)
        {
            int result = -1;
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select count(IdTreninga) as broj from trening where year(DanIVrijemeOdrzavanja) = @year  and Korisnik_KorisnickoIme=@username and JeAktivan=1 ;";
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Parameters.AddWithValue("@year", DateTime.Now.Year);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    result = reader.GetInt32("broj");
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return result;
        }
        public int GetNumberOfTreningPreviousYear(String username)
        {
            int result = -1;
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            DateTime dateTime = DateTime.Now;
            dateTime = dateTime.AddYears(-1);
            mySqlCommand.CommandText = "select count(IdTreninga) as broj from trening where year(DanIVrijemeOdrzavanja) = @year and Korisnik_KorisnickoIme=@username  and JeAktivan=1;";
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Parameters.AddWithValue("@year", dateTime.Year);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    result = reader.GetInt32("broj");
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return result;
        }
        public int GetSpentTimeThisMonth(String username)
        {
            int result = -1;
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select sum(Trajanje) as broj from trening where year(DanIVrijemeOdrzavanja) = @year and month(DanIVrijemeOdrzavanja)=@month and Korisnik_KorisnickoIme=@username  and JeAktivan=1;";
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Parameters.AddWithValue("@year", DateTime.Now.Year);
            mySqlCommand.Parameters.AddWithValue("@month", DateTime.Now.Month);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    result = reader.IsDBNull(0) ? 0 : reader.GetInt32("broj");
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return result;
        }
        public int GetSpentTimePreviousMonth(String username)
        {
            int result = -1;
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            DateTime dateTime = DateTime.Now;
            dateTime = dateTime.AddMonths(-1);
            mySqlCommand.CommandText = "select sum(Trajanje) as broj from trening where year(DanIVrijemeOdrzavanja) = @year and month(DanIVrijemeOdrzavanja)=@month and Korisnik_KorisnickoIme=@username  and JeAktivan=1 ;";
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Parameters.AddWithValue("@year", dateTime.Year);
            mySqlCommand.Parameters.AddWithValue("@month", dateTime.Month);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    result = reader.IsDBNull(0) ? 0 : reader.GetInt32("broj");
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return result;
        }
        public int GetSpentTimeThisYear(String username)
        {
            int result = -1;
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select sum(Trajanje) as broj from trening where year(DanIVrijemeOdrzavanja) = @year  and Korisnik_KorisnickoIme=@username and JeAktivan=1 ;";
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Parameters.AddWithValue("@year", DateTime.Now.Year);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    result = reader.IsDBNull(0) ? 0 : reader.GetInt32("broj");
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return result;
        }
        public int GetSpentTimePreviousYear(String username)
        {
            int result = -1;
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            DateTime dateTime = DateTime.Now;
            dateTime = dateTime.AddYears(-1);
            mySqlCommand.CommandText = "select sum(Trajanje) as broj from trening where year(DanIVrijemeOdrzavanja) = @year and Korisnik_KorisnickoIme=@username  and JeAktivan=1 ;";
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Parameters.AddWithValue("@year", dateTime.Year);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    result = reader.IsDBNull(0) ? 0: reader.GetInt32("broj");
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return result;
        }
        public Tuple<int,int> GetNumberOfSetsAndRepsThisMonth(String username)
        {
            int reps = 0, sets = 0;
            List<Training> trainings = GetIdTrainingThisMonth(username);
            foreach(var training in trainings)
            {
                List<ExerciseOnTrening> exerciseOnTrenings = GetExerciseOnTeningById(training.Id);
                foreach(ExerciseOnTrening exercise in exerciseOnTrenings)
                {
                    reps += exercise.Reps * exercise.Sets;
                    sets += exercise.Sets;
                }
            }
            return new Tuple<int, int>(reps,sets);
        }
        public Tuple<int, int> GetNumberOfSetsAndRepsPreviousYear(String username)
        {
            int reps = 0, sets = 0;
            List<Training> trainings = GetIdTrainingPreviousYear(username);
            foreach (var training in trainings)
            {
                List<ExerciseOnTrening> exerciseOnTrenings = GetExerciseOnTeningById(training.Id);
                foreach (ExerciseOnTrening exercise in exerciseOnTrenings)
                {
                    reps += exercise.Reps * exercise.Sets;
                    sets += exercise.Sets;
                }
            }
            return new Tuple<int, int>(reps, sets);
        }
        public Tuple<int, int> GetNumberOfSetsAndRepsPreviousMonth(String username)
        {
            int reps = 0, sets = 0;
            List<Training> trainings = GetIdTrainingPreviousMonth(username);
            foreach (var training in trainings)
            {
                List<ExerciseOnTrening> exerciseOnTrenings = GetExerciseOnTeningById(training.Id);
                foreach (ExerciseOnTrening exercise in exerciseOnTrenings)
                {
                    reps += exercise.Reps * exercise.Sets;
                    sets += exercise.Sets;
                }
            }
            return new Tuple<int, int>(reps, sets);
        }
        public Tuple<int, int> GetNumberOfSetsAndRepsThisYear(String username)
        {
            int reps = 0, sets = 0;
            List<Training> trainings = GetIdTrainingThisYear(username);
            foreach (var training in trainings)
            {
                List<ExerciseOnTrening> exerciseOnTrenings = GetExerciseOnTeningById(training.Id);
                foreach (ExerciseOnTrening exercise in exerciseOnTrenings)
                {
                    reps += exercise.Reps * exercise.Sets;
                    sets += exercise.Sets;
                }
            }
            return new Tuple<int, int>(reps, sets);
        }
        private List<Training> GetIdTrainingThisMonth(String username)
        {
            List<Training> trainings = new List<Training>();
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select IdTreninga from trening where year(DanIVrijemeOdrzavanja) = @year and month(DanIVrijemeOdrzavanja)=@month and Korisnik_KorisnickoIme=@username  and JeAktivan=1;";
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Parameters.AddWithValue("@year", DateTime.Now.Year);
            mySqlCommand.Parameters.AddWithValue("@month", DateTime.Now.Month);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    trainings.Add(new Training
                    {
                        Id = reader.GetInt32("IdTreninga")
                    });
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return trainings;
        }
        private List<Training> GetIdTrainingPreviousMonth(String username)
        {
            List<Training> trainings = new List<Training>();
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            DateTime dateTime = DateTime.Now.AddMonths(-1);
            mySqlCommand.CommandText = "select IdTreninga from trening where year(DanIVrijemeOdrzavanja) = @year and month(DanIVrijemeOdrzavanja)=@month and Korisnik_KorisnickoIme=@username  and JeAktivan=1;";
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Parameters.AddWithValue("@year", dateTime.Year);
            mySqlCommand.Parameters.AddWithValue("@month", dateTime.Month);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    trainings.Add(new Training
                    {
                        Id = reader.GetInt32("IdTreninga")
                    });
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return trainings;
        }
        private List<Training> GetIdTrainingPreviousYear(String username)
        {
            List<Training> trainings = new List<Training>();
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            DateTime dateTime = DateTime.Now.AddYears(-1);
            mySqlCommand.CommandText = "select IdTreninga from trening where year(DanIVrijemeOdrzavanja) = @year  and Korisnik_KorisnickoIme=@username  and JeAktivan=1;";
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Parameters.AddWithValue("@year", dateTime.Year);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    trainings.Add(new Training
                    {
                        Id = reader.GetInt32("IdTreninga")
                    });
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return trainings;
        }
        private List<Training> GetIdTrainingThisYear(String username)
        {
            List<Training> trainings = new List<Training>();
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            DateTime dateTime = DateTime.Now;
            mySqlCommand.CommandText = "select IdTreninga from trening where year(DanIVrijemeOdrzavanja) = @year  and Korisnik_KorisnickoIme=@username  and JeAktivan=1;";
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Parameters.AddWithValue("@year", dateTime.Year);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    trainings.Add(new Training
                    {
                        Id = reader.GetInt32("IdTreninga")
                    });
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return trainings;
        }
        public List<ExerciseOnTrening> GetPersonalBestRecordAllExercise(String username)
        {
            List<ExerciseOnTrening> exerciseOnTrenings = new List<ExerciseOnTrening>();
            Dictionary<int, int> records = new ExerciseDAO().GetExercisesID();
            List<Training> trainings = GetAllTrainingsID(username);
            foreach (Training training in trainings)
            {
                List<ExerciseOnTrening> exercisesOnTrening = GetExerciseOnTeningIdAndRepsById(training.Id);
                foreach (ExerciseOnTrening exercise in exercisesOnTrening)
                {
                    if (exercise.Reps > records[exercise.Id])
                        records[exercise.Id] = exercise.Reps;
                }
            }
            ExerciseDAO exerciseDAO = new ExerciseDAO();
            records.Keys.ToList<int>().ForEach(e =>
            {
                if(records[e] > 0)
                {
                    Exercise exercise = exerciseDAO.GetExerciseById(e);
                    exerciseOnTrenings.Add(new ExerciseOnTrening
                    {
                        Name = exercise.Name,
                        Level = exercise.Level,
                        Type = exercise.Type,
                        Weighted = exercise.Weighted,
                        Reps = records[e]
                    });
                }
            });

            return exerciseOnTrenings;
        }
        public List<Training> GetAllTrainingsID(String username)
        {
            List<Training> trainings = new List<Training>();
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select IdTreninga from trening where Korisnik_KorisnickoIme=@username and JeAktivan=1 ;";
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    trainings.Add(new Training
                    {
                        Id = reader.GetInt32("IdTreninga")
                    }); ;
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return trainings;
        }
        public List<ExerciseOnTrening> GetExerciseOnTeningIdAndRepsById(int id)
        {
            List<ExerciseOnTrening> exercises = new List<ExerciseOnTrening>();
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select Vjezba_IdVjezbe,BrojPonavljanja from vjezbanatreningView where Trening_IdTreninga=@id;";
            mySqlCommand.Parameters.AddWithValue("@id", id);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    exercises.Add(new ExerciseOnTrening
                    {
                        Id = reader.GetInt32("Vjezba_IdVjezbe"),                      
                        Reps = reader.GetInt32("BrojPonavljanja")
                    });
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return exercises;
        }
        public bool UpadateTrening(Training trening)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "update trening set Trajanje=@spentTime , DanIVrijemeOdrzavanja =  @dateTime, TipMjesta =@locationType , Mjesto_PostanskiBroj = @postCode , Mjesto_Ulica = @street where IdTreninga=@id;";
            mySqlCommand.Parameters.AddWithValue("@spentTime", trening.SpentTime);
            mySqlCommand.Parameters.AddWithValue("@dateTime", trening.DateTime);
            mySqlCommand.Parameters.AddWithValue("@locationType", trening.LocationType);
            mySqlCommand.Parameters.AddWithValue("@postCode", trening.PostCode);
            mySqlCommand.Parameters.AddWithValue("@street", trening.Street);
            mySqlCommand.Parameters.AddWithValue("@id", trening.Id);
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                return true;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return false;
        }
    }
}
