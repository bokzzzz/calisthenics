﻿using Calisthenics_Trening_Network.DB;
using CalisthenicsTreningNetworkHCI.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalisthenicsTreningNetworkHCI.DAO
{
    class CommentDAO
    {
        public bool AddComment(Comment comment)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "insert into komentarisetrening (Tekst,Vrijeme,Korisnik_KorisnickoIme,Trening_IdTreninga) values" +
                "(@text,@dateTime,@creator,@id)";
            mySqlCommand.Parameters.AddWithValue("@text", comment.Text);
            mySqlCommand.Parameters.AddWithValue("@dateTime", comment.DateTime);
            mySqlCommand.Parameters.AddWithValue("@creator", comment.Creator);
            mySqlCommand.Parameters.AddWithValue("@id", comment.Id);
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (MySqlException)
            {
                return false;
            }
        }
    }
}
