﻿using Calisthenics_Trening_Network.DB;
using CalisthenicsTreningNetworkHCI.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalisthenicsTreningNetworkHCI.DAO
{
    class ExerciseOnTreningDAO
    {
        public bool AddExerciseOnTrening(ExerciseOnTrening exercise,int idTrening)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "Insert into vjezbanatreningu(Setovi, BrojPonavljanja, OdmorIzmedjuSetova, Vjezba_IdVjezbe, Trening_IdTreninga) values(" +
                "@sets, @reps, @rest, @idExercise, @IdTrening) ;";
            mySqlCommand.Parameters.AddWithValue("@sets", exercise.Sets);
            mySqlCommand.Parameters.AddWithValue("@reps", exercise.Reps);
            mySqlCommand.Parameters.AddWithValue("@rest", exercise.Rest);
            mySqlCommand.Parameters.AddWithValue("@idExercise", exercise.Id);
            mySqlCommand.Parameters.AddWithValue("@IdTrening", idTrening);

            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                return true;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return false;
        }
        public bool RemoveExerciseOnTrening(ExerciseOnTrening exercise, int idTrening)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "delete from vjezbanatreningu where Vjezba_IdVjezbe =  @idExercise and Trening_IdTreninga = @IdTrening and BrojPonavljanja = @reps ;";
            mySqlCommand.Parameters.AddWithValue("@reps", exercise.Reps);
            mySqlCommand.Parameters.AddWithValue("@idExercise", exercise.Id);
            mySqlCommand.Parameters.AddWithValue("@IdTrening", idTrening);
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                return true;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return false;
        }
     
    }
}
