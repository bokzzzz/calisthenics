﻿using Calisthenics_Trening_Network.DB;
using HCI_Calisthenics_Trening.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calisthenics_Trening_Network.DAO
{
    class UserDAO
    {
        public User GetUser(User user)
        {
            User userRet = null;
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand= connection.CreateCommand();
            mySqlCommand.CommandText = "select * from korisnik where KorisnickoIme=@username and JeAktivan=1";
            mySqlCommand.Parameters.AddWithValue("@username", user.Username);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    userRet = new User
                    {
                        Username = user.Username,
                        Language=reader.GetString("Jezik"),
                        Theme=reader.GetString("Tema"),
                        Name=reader.GetString("Ime"),
                        Surname=reader.GetString("Prezime"),
                        Type=reader.GetString("Tip")
                        
                    };
                }
                reader.Close();
            }catch(MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return userRet;
        }
        public int IsUserExist(User user)
        {
            int userRet = 2;
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select count(*) as broj from korisnik where KorisnickoIme=@username and sifra=@pass and JeAktivan=1";
            mySqlCommand.Parameters.AddWithValue("@username", user.Username);
            mySqlCommand.Parameters.AddWithValue("@pass", user.Password);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    userRet = reader.GetInt32("broj");
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return userRet;
        }
        public int isUsernameExist(User user)
        {
            int userRet = 2;
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "select count(*) as broj from korisnik where KorisnickoIme=@username";
            mySqlCommand.Parameters.AddWithValue("@username", user.Username);
            mySqlCommand.Prepare();
            try
            {
                MySqlDataReader reader = mySqlCommand.ExecuteReader();
                if (reader.Read())
                {
                    userRet = reader.GetInt32("broj");
                }
                reader.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.StackTrace);
            }
            connection.Close();
            return userRet;
        }
        public bool AddUser(User user)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "insert into korisnik (KorisnickoIme,Ime,Prezime,Sifra,Tema,Jezik) values" +
                "(@username,@name,@surname,@pass,@theme,@lang)";
            mySqlCommand.Parameters.AddWithValue("@name", user.Name);
            mySqlCommand.Parameters.AddWithValue("@username", user.Username);
            mySqlCommand.Parameters.AddWithValue("@surname", user.Surname);
            mySqlCommand.Parameters.AddWithValue("@pass", user.Password);
            mySqlCommand.Parameters.AddWithValue("@theme", user.Theme);
            mySqlCommand.Parameters.AddWithValue("@lang", user.Language);
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (MySqlException)
            {
                return false;
            }
        }
        public bool ChangeName(String name,String username)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "update korisnik  set Ime = @name where KorisnickoIme =@username ";
            mySqlCommand.Parameters.AddWithValue("@name", name);
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (MySqlException)
            {
                return false;
            }
        }
        public bool ChangeSurname(String surname, String username)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "update korisnik  set Prezime = @surname where KorisnickoIme =@username ";
            mySqlCommand.Parameters.AddWithValue("@surname", surname);
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (MySqlException)
            {
                return false;
            }
        }
        public bool ChangeTheme(String theme, String username)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "update korisnik  set Tema = @theme where KorisnickoIme =@username ";
            mySqlCommand.Parameters.AddWithValue("@theme", theme);
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (MySqlException)
            {
                return false;
            }
        }
        public bool ChangeLanguage(String language, String username)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "update korisnik  set Jezik = @language where KorisnickoIme =@username ";
            mySqlCommand.Parameters.AddWithValue("@language", language);
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (MySqlException)
            {
                return false;
            }
        }
        public bool ChangePassword(String password, String username)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "update korisnik  set Sifra = @pass where KorisnickoIme = @username ";
            mySqlCommand.Parameters.AddWithValue("@pass", password);
            mySqlCommand.Parameters.AddWithValue("@username", username);
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (MySqlException)
            {
                return false;
            }
        }
        public bool AddAdmin(User user)
        {
            MySqlConnection connection = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = "insert into korisnik (KorisnickoIme,Ime,Prezime,Sifra,Tema,Jezik,Tip) values" +
                "(@username,@name,@surname,@pass,@theme,@lang,@type)";
            mySqlCommand.Parameters.AddWithValue("@name", user.Name);
            mySqlCommand.Parameters.AddWithValue("@username", user.Username);
            mySqlCommand.Parameters.AddWithValue("@surname", user.Surname);
            mySqlCommand.Parameters.AddWithValue("@pass", user.Password);
            mySqlCommand.Parameters.AddWithValue("@theme", user.Theme);
            mySqlCommand.Parameters.AddWithValue("@lang", user.Language);
            mySqlCommand.Parameters.AddWithValue("@type", "admin");
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (MySqlException)
            {
                return false;
            }
        }
    }
}
