﻿#pragma checksum "..\..\..\GUI\ChangeTrainingWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "665CAE230C85429D1D86CE3109748D14F125AB8774B10CE1DE9159658406562C"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using CalisthenicsTreningNetworkHCI.GUI;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Chromes;
using Xceed.Wpf.Toolkit.Core.Converters;
using Xceed.Wpf.Toolkit.Core.Input;
using Xceed.Wpf.Toolkit.Core.Media;
using Xceed.Wpf.Toolkit.Core.Utilities;
using Xceed.Wpf.Toolkit.Panels;
using Xceed.Wpf.Toolkit.Primitives;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Commands;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Xceed.Wpf.Toolkit.Zoombox;


namespace CalisthenicsTreningNetworkHCI.GUI {
    
    
    /// <summary>
    /// ChangeTrainingWindow
    /// </summary>
    public partial class ChangeTrainingWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 17 "..\..\..\GUI\ChangeTrainingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid addTreningGrid;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\GUI\ChangeTrainingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox locationTypeTextBox;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\GUI\ChangeTrainingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox spentTimeTextBox;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\GUI\ChangeTrainingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DateTimePicker dateTimePicker;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\GUI\ChangeTrainingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid locationDataGrid;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\GUI\ChangeTrainingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid exerviseGrid;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\GUI\ChangeTrainingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button addExerciseButton;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\GUI\ChangeTrainingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ChangeExerciseButton;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\..\GUI\ChangeTrainingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button removeExerciseButton;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\GUI\ChangeTrainingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button addTreningButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/CalisthenicsTreningNetworkHCI;component/gui/changetrainingwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\GUI\ChangeTrainingWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.addTreningGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.locationTypeTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.spentTimeTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 40 "..\..\..\GUI\ChangeTrainingWindow.xaml"
            this.spentTimeTextBox.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.NumberValidationTextBox);
            
            #line default
            #line hidden
            return;
            case 4:
            this.dateTimePicker = ((Xceed.Wpf.Toolkit.DateTimePicker)(target));
            return;
            case 5:
            this.locationDataGrid = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 6:
            this.exerviseGrid = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 7:
            this.addExerciseButton = ((System.Windows.Controls.Button)(target));
            
            #line 69 "..\..\..\GUI\ChangeTrainingWindow.xaml"
            this.addExerciseButton.Click += new System.Windows.RoutedEventHandler(this.addExerciseButton_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.ChangeExerciseButton = ((System.Windows.Controls.Button)(target));
            
            #line 70 "..\..\..\GUI\ChangeTrainingWindow.xaml"
            this.ChangeExerciseButton.Click += new System.Windows.RoutedEventHandler(this.ChangeExerciseButton_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.removeExerciseButton = ((System.Windows.Controls.Button)(target));
            
            #line 71 "..\..\..\GUI\ChangeTrainingWindow.xaml"
            this.removeExerciseButton.Click += new System.Windows.RoutedEventHandler(this.removeExerciseButton_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.addTreningButton = ((System.Windows.Controls.Button)(target));
            
            #line 79 "..\..\..\GUI\ChangeTrainingWindow.xaml"
            this.addTreningButton.Click += new System.Windows.RoutedEventHandler(this.addTreningButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

