﻿#pragma checksum "..\..\..\GUI\AddChangeLocation.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "EBA639263F31C55A23B2729F26584BBE448CC1003C49D042131F745921D45F66"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using CalisthenicsTreningNetworkHCI.GUI;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace CalisthenicsTreningNetworkHCI.GUI {
    
    
    /// <summary>
    /// AddChangeLocation
    /// </summary>
    public partial class AddChangeLocation : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 27 "..\..\..\GUI\AddChangeLocation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox postCodeTextBox;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\GUI\AddChangeLocation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox streetTextBox;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\GUI\AddChangeLocation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox nameTextBox;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\GUI\AddChangeLocation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button addLocationButton;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\GUI\AddChangeLocation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button saveLocationButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/CalisthenicsTreningNetworkHCI;component/gui/addchangelocation.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\GUI\AddChangeLocation.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.postCodeTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 27 "..\..\..\GUI\AddChangeLocation.xaml"
            this.postCodeTextBox.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.postCodeTextBox_PreviewTextInput);
            
            #line default
            #line hidden
            return;
            case 2:
            this.streetTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.nameTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.addLocationButton = ((System.Windows.Controls.Button)(target));
            
            #line 38 "..\..\..\GUI\AddChangeLocation.xaml"
            this.addLocationButton.Click += new System.Windows.RoutedEventHandler(this.addLocationButton_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.saveLocationButton = ((System.Windows.Controls.Button)(target));
            
            #line 39 "..\..\..\GUI\AddChangeLocation.xaml"
            this.saveLocationButton.Click += new System.Windows.RoutedEventHandler(this.saveLocationButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

