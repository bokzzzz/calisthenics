﻿using Calisthenics_Trening_Network.DAO;
using Calisthenics_Trening_Network.GUI;
using HCI_Calisthenics_Trening.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HCI_Calisthenics_Trening
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            String username = usernameTextBox.Text;
            String password = passwordPasswordBox.Password;
            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password))
            {
                MessageBox.Show("Please, fill the fields !", "Login Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            UserDAO userDAO = new UserDAO();
            User user = new User { Username = username, Password = password };
            int isExist = userDAO.IsUserExist(user);
            if (isExist == 1)
            {
                User userFull = userDAO.GetUser(user);
                if ("klasicni".Equals(userFull.Type))
                {
                    this.Hide();
                    Window window = new UserWindow(userFull);
                    window.ShowDialog();
                    bool isClosed = window.DialogResult.Value;
                    if(isClosed)
                    this.Show();

                }
                else if ("admin".Equals(userFull.Type))
                {
                    this.Hide();
                    Window window = new AdminWindow(userFull);
                    window.ShowDialog();
                    bool isClosed = window.DialogResult.Value;
                    if (isClosed)
                        this.Show();

                }
                else
                {
                    MessageBox.Show("Please, login later!", "Login Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else if (isExist == 0)
            {
                MessageBox.Show("Username or password is incorrect!", "Login Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                MessageBox.Show("Please, login later!", "Login Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void signupButton_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            if (new SignUpWindow().ShowDialog().Value)
                this.Show();
        }


        private void usernameTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                loginButton_Click(null, null);
        }
    }
}
