-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: calisthenics
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `komentarisetrening`
--

LOCK TABLES `komentarisetrening` WRITE;
/*!40000 ALTER TABLE `komentarisetrening` DISABLE KEYS */;
INSERT INTO `komentarisetrening` VALUES ('Mogos ti jos jedan','2020-04-18 14:06:58','sinisa',18),('Kurcu tebi ovo ne valja ','2020-04-18 17:06:58','djoks',18),('Ma da druze','2020-04-18 18:35:05','test',18),('Ma zezam se ja','2020-04-19 14:12:01','djoks',18);
/*!40000 ALTER TABLE `komentarisetrening` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `korisnik`
--

LOCK TABLES `korisnik` WRITE;
/*!40000 ALTER TABLE `korisnik` DISABLE KEYS */;
INSERT INTO `korisnik` VALUES ('baks','Bakac','Bakčević','baks','Српски','Dark blue theme',1,'klasicni'),('djoks','Djordje','Bojic','djoks','Srpski','Classic theme',1,'klasicni'),('sinisa','Sinisa','Bojic','sis','English','Classic theme',1,'klasicni'),('tes','tes','tes','tes','Srpski','Dark blue theme',1,'admin'),('test','Testnoa','Testnoee','test','English','Dark theme',1,'admin');
/*!40000 ALTER TABLE `korisnik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mjesto`
--

LOCK TABLES `mjesto` WRITE;
/*!40000 ALTER TABLE `mjesto` DISABLE KEYS */;
INSERT INTO `mjesto` VALUES (71370,'Gradski Stadion Breza FK Rudar','Alije Izetbegovića',1),(75000,'Slatina','Albina i Franje Herljevića',1),(75000,'Ambulanta Mikrostanica','Aleja Alije Izetbegovića',1),(75000,'Školsko igralište-Zlokovac','Dr Tihomila Markovića',1),(78000,'Park Mladen Stojanović','Dr. Mladena Stojanovića',1),(78000,'K4','Majke Jugovića',1),(78000,'Dvorište Gimnazije','Zmaj Jovina',1),(78220,'Stadion FK Mladost','Cara Dušana',1),(78223,'ROS Brothers','Bašče bb',1),(78223,'tes','Bašče bbb',0),(78224,'baki 23','Bašče bb',1),(78430,'Centar srednjih škola \"Ivo Andrić\"','Rade Vranješević',1),(88000,'Univerzitet Džemal Bijedić','H. Zahirovića Iace',1);
/*!40000 ALTER TABLE `mjesto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ocjenjujetrening`
--

LOCK TABLES `ocjenjujetrening` WRITE;
/*!40000 ALTER TABLE `ocjenjujetrening` DISABLE KEYS */;
INSERT INTO `ocjenjujetrening` VALUES (9,18,'baks'),(10,18,'djoks'),(8,19,'djoks');
/*!40000 ALTER TABLE `ocjenjujetrening` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `prisustvo`
--

LOCK TABLES `prisustvo` WRITE;
/*!40000 ALTER TABLE `prisustvo` DISABLE KEYS */;
/*!40000 ALTER TABLE `prisustvo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `trening`
--

LOCK TABLES `trening` WRITE;
/*!40000 ALTER TABLE `trening` DISABLE KEYS */;
INSERT INTO `trening` VALUES (50,'2020-04-15 14:30:15','šuma',18,78223,'Bašče bb','baks',1),(120,'2020-04-16 11:30:08','šuma',19,78223,'Bašče bb','baks',1),(100,'2020-04-16 11:30:08','šuma',20,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',21,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',22,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',23,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',24,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',25,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',26,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',27,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',28,78223,'Bašče bb','test',0),(100,'2020-04-16 11:30:08','šuma',29,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',30,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',31,78223,'Bašče bb','test',1),(100,'2020-03-16 11:30:08','šuma',32,78223,'Bašče bb','test',1),(100,'2020-03-16 11:30:08','šuma',33,78223,'Bašče bb','test',1),(100,'2020-03-16 11:30:08','šuma',34,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',35,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',36,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',37,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',38,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',39,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',40,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',41,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',42,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',43,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',44,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',45,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',46,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',47,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',48,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',49,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',50,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',51,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',52,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',53,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',54,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',55,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',56,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',57,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',58,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',59,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',60,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',61,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',62,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',63,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',64,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',65,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',66,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',67,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',68,78223,'Bašče bb','test',1),(100,'2019-04-16 11:30:08','šuma',69,78223,'Bašče bb','test',1),(100,'2019-04-16 11:30:08','šuma',70,78223,'Bašče bb','test',1),(100,'2019-04-16 11:30:08','šuma',71,78223,'Bašče bb','test',1),(100,'2019-04-16 11:30:00','šumae',72,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',73,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',74,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',75,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',76,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',77,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',78,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',79,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',80,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',81,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',82,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',83,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',84,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',85,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',86,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',87,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',88,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',89,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',90,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',91,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',92,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',93,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',94,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',95,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',96,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',97,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',98,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',99,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',100,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',101,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',102,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',103,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',104,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',105,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',106,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',107,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',108,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',109,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',110,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',111,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',112,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',113,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',114,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',115,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',116,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',117,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',118,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',119,78223,'Bašče bb','test',1),(100,'2020-04-16 11:30:08','šuma',120,78223,'Bašče bb','test',1),(120,'2020-04-24 22:28:34','Šuma',121,78223,'Bašče bb','baks',1),(120,'2020-04-26 16:23:44','Šuma',122,78223,'Bašče bb','baks',1),(100,'2020-04-23 16:23:44','Šuma',123,78223,'Bašče bb','baks',1),(100,'2020-04-20 16:23:44','šuma',124,78223,'Bašče bb','baks',1),(100,'2020-04-22 16:23:44','šuma',125,78223,'Bašče bb','baks',1),(120,'2020-04-19 16:23:44','šuma',126,78223,'Bašče bb','baks',1),(1500,'2020-04-01 15:15:00','sadae',127,78223,'Bašče bb','test',1),(120,'2020-04-30 14:12:00','Šuma',128,78223,'Bašče bb','baks',1),(50,'2020-05-07 18:02:00','šuma',129,78223,'Bašče bb','baks',1),(50,'2020-05-08 18:04:00','šuma',130,78223,'Bašče bb','baks',1),(120,'2020-05-01 18:05:00','šuma',131,78223,'Bašče bb','baks',1);
/*!40000 ALTER TABLE `trening` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `vjezba`
--

LOCK TABLES `vjezba` WRITE;
/*!40000 ALTER TABLE `vjezba` DISABLE KEYS */;
INSERT INTO `vjezba` VALUES (5,'Dip',0,1,'Push',1),(6,'Dip',32,2,'Push',1),(8,'Dip',60,3,'Push',1),(9,'Handstand push up',0,3,'Push',1),(10,'Muscle up',0,2,'Pull',1),(11,'Muscle up',32,4,'Pull',1),(12,'Planche push up',0,4,'Push',1),(14,'Pull up',0,1,'Pull',1),(15,'Pull up',20,2,'Pull',1),(16,'Pull up',32,3,'Pull',1),(17,'Pull up',40,4,'Pull',1),(18,'Front level pull up',0,4,'Pull',1),(20,'Dip',50,4,'Push',1),(24,'Trbušnjak',10,1,'Pull',1),(25,'test',150,3,'Push',0),(26,'baki',1,3,'Push',0),(27,'Dip',80,4,'Push',1),(28,'Dip',100,4,'Push',1),(29,'Dip',70,4,'Push',1);
/*!40000 ALTER TABLE `vjezba` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `vjezbanatreningu`
--

LOCK TABLES `vjezbanatreningu` WRITE;
/*!40000 ALTER TABLE `vjezbanatreningu` DISABLE KEYS */;
INSERT INTO `vjezbanatreningu` VALUES (5,5,5,6,72),(5,5,5,6,127),(1,11,120,8,121),(2,7,120,8,122),(1,11,120,8,122),(1,12,120,8,122),(1,13,120,8,122),(1,9,120,8,125),(3,10,120,8,125),(1,11,120,8,125),(1,1,100,15,123),(1,2,100,15,123),(1,3,100,15,123),(1,4,100,15,123),(1,5,100,15,123),(1,6,100,15,123),(2,5,180,16,19),(5,6,180,16,19),(1,10,120,16,121),(1,1,100,16,123),(1,2,100,16,123),(1,3,100,16,123),(1,4,100,16,123),(1,5,100,16,123),(1,6,100,16,123),(1,7,100,16,123),(10,5,120,16,124),(5,9,180,16,129),(1,10,60,20,18),(1,12,60,20,18),(1,13,60,20,18),(1,15,60,20,18),(1,10,60,20,33),(1,10,60,20,69),(1,19,120,20,121),(5,10,120,20,126),(2,20,150,20,130),(6,15,210,20,131),(4,25,120,24,123),(4,25,120,24,124),(1,4,60,27,128),(1,1,120,28,128),(1,10,120,29,130);
/*!40000 ALTER TABLE `vjezbanatreningu` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-12 20:57:41
